#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "utils.h"
#include "Acceleration.h"
#include "Displacement.h"
#include "Speed.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    Utils utils{};
    std::cout<<"TEST GUI\n";
        {
            observer::Displacement displacement;

            observer::Speed speed;
            observer::Acceleration acceleration;

            displacement.registerObserver(&speed);
            displacement.registerObserver(&acceleration);

            displacement.set(10);
            displacement.set(11);
            displacement.set(13);

            std::cout << "Result: speed: " << speed.get() << ", acceleration: " << acceleration.get() <<"\n";
        }
        std::cout<<"\nMy tests: \n";
        {
            observer::Displacement displacement1;

            observer::Speed speed1;
            observer::Acceleration acceleration1;

            displacement1.registerObserver(&speed1);
            displacement1.registerObserver(&acceleration1);

            displacement1.set(2);
            std::cout << "Result: speed: " << speed1.get() << ", acceleration: " << acceleration1.get() <<"\n";
            displacement1.set(1);
            std::cout << "Result: speed: " << speed1.get() << ", acceleration: " << acceleration1.get() <<"\n";
            displacement1.set(3);
            std::cout << "Result: speed: " << speed1.get() << ", acceleration: " << acceleration1.get() <<"\n";
            displacement1.set(7);
            std::cout << "Result: speed: " << speed1.get() << ", acceleration: " << acceleration1.get() <<"\n";
            displacement1.unregisterObserver(&speed1);
            displacement1.unregisterObserver(&acceleration1);
        }
}
