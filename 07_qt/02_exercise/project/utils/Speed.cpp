//
// Created by student on 12/4/20.
//
#include "Speed.h"

float observer::Speed::get() {
    if(hasFirstValue && hasSecondValue)
        return SecondValue-FirstValue;
    return 0;
}

observer::Speed::Speed() {
    hasSecondValue=false;
    hasFirstValue=false;
}

void::observer::Speed::notify(float a){
    if(!hasFirstValue)
    {
        hasFirstValue=true;
        FirstValue=a;
    }
    else if(!hasSecondValue)
    {
        hasSecondValue=true;
        SecondValue=a;
    }
    else
    {
        FirstValue=SecondValue;
        SecondValue=a;
    }
}
