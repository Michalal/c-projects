//
// Created by student on 12/4/20.
//

#ifndef PROJECT_OBSERVABLE_H
#define PROJECT_OBSERVABLE_H
#include <iostream>
#include <set>
#include "Observer.h"
namespace observer{
    class Observable{
    public:
        void registerObserver(Observer*);
        void unregisterObserver(Observer*);
    protected:
        void notifyObservers(float);
    private:
        std::set<Observer*>observers;
    };
}
#endif //PROJECT_OBSERVABLE_H
