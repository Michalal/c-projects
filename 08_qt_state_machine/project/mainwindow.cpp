#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QStateMachine>
#include <QHistoryState>
#include <QFileDialog>
#include <QDebug>
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // TODO: Create state machine
    auto stateMachine=new QStateMachine{this};

    // TODO: Create states
    auto Unlocked=new QState{stateMachine};
    auto Locked=new QState{stateMachine};
    auto Startup=new QState{Unlocked};
    auto Open=new QState{Unlocked};
    auto Error=new QState{Unlocked};
    auto View=new QState{Unlocked};
    auto Save=new QState{Unlocked};
    auto Edit=new QState{Unlocked};
    QHistoryState* Temp=new QHistoryState(Unlocked);

    // TODO: Set appropriate 'assignProperty'
    Unlocked->assignProperty(ui->pbToggle, "text", "Lock");
    Unlocked->assignProperty(ui->pbOpen, "enabled", true);
    Unlocked->assignProperty(ui->pbSave, "enabled", false);
    Unlocked->assignProperty(ui->teText, "enabled", false);

    Locked->assignProperty(ui->pbToggle, "text", "Unlock");
    Locked->assignProperty(ui->pbOpen, "enabled", false);
    Locked->assignProperty(ui->pbSave, "enabled", false);
    Locked->assignProperty(ui->teText, "enabled", false);

    Startup->assignProperty(ui->pbOpen, "enabled", true);
    Startup->assignProperty(ui->pbSave, "enabled", false);
    Startup->assignProperty(ui->teText, "enabled", false);
    Startup->assignProperty(ui->teText, "placeholderText", "Open file to start editing...");

    View->assignProperty(ui->pbOpen, "enabled", true);
    View->assignProperty(ui->pbSave, "enabled", false);
    View->assignProperty(ui->teText, "enabled", true);

    Edit->assignProperty(ui->pbOpen, "enabled", false);
    Edit->assignProperty(ui->pbSave, "enabled", true);
    Edit->assignProperty(ui->teText, "enabled", true);

    Error->assignProperty(ui->pbOpen, "enabled", true);
    Error->assignProperty(ui->pbSave, "enabled", false);
    Error->assignProperty(ui->teText, "enabled", false);
    Error->assignProperty(ui->teText, "placeholderText", "Error occured. Open file to start editing...");

    // TODO: Set state transitions including this class events and slots
    Unlocked->addTransition(ui->pbToggle, SIGNAL(clicked()), Locked);

    Locked->addTransition(ui->pbToggle, SIGNAL(clicked()), Temp);

    Startup->addTransition(ui->pbToggle, SIGNAL(clicked()), Locked);
    Startup->addTransition(ui->pbOpen, SIGNAL(clicked()), Open);
    connect(Open, SIGNAL(entered()), this, SLOT(open()));

    Open->addTransition(this, SIGNAL(error()), Error);
    Open->addTransition(this, SIGNAL(opened()), View);

    Error->addTransition(ui->pbOpen, SIGNAL(clicked()), Open);

    View->addTransition(ui->teText, SIGNAL(textChanged()), Edit);
    View->addTransition(ui->pbOpen, SIGNAL(clicked()), Open);

    Edit->addTransition(ui->pbSave, SIGNAL(clicked()), Save);
    connect(Save, SIGNAL(entered()), this, SLOT(save()));

    Save->addTransition(this, SIGNAL(error()), Error);
    Save->addTransition(this, SIGNAL(saved()), View);

    // TODO: Set initial state
    stateMachine->setInitialState(Unlocked);
    Unlocked->setInitialState(Startup);

    // TODO: Start state machine
    stateMachine->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::open()
{
    // TODO: Show file dialog
    fileName=QFileDialog::getOpenFileName(this, tr("Open File"), "/home", tr("*.txt"));

    // TODO: Open selected file
    QFile file(fileName);

    // TODO: Emit 'error' if opening failed
    if(!file.open(QIODevice::ReadWrite | QIODevice::Text))
        emit error();

    // TODO: Set text and emit 'opened' if suceeded
    else
    {
        QTextStream stream(&file);
        QString read_text=stream.readAll();
        ui->teText->setText(read_text);
        emit opened();
    }

    // TODO: Save file name in 'fileName'
    file.close();
}

void MainWindow::save()
{
    // TODO: Open 'fileName' for writing
    QFile file(fileName);

    // TODO: Emit 'error' if opening failed
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        emit error();

    // TODO: Save file and emit 'saved' if succeeded
    else
    {
        QTextStream stream(&file);
        QString save_text=ui->teText->toPlainText();
        stream << save_text;
        emit saved();
    }
    file.close();
}
