//
// Created by student on 12/4/20.
//
#include "factory/executor/AnisotropicCudaCore.h"

factory::executor::AnisotropicCudaCore::AnisotropicCudaCore(int gpuId) :gpuId(gpuId){}

void factory::executor::AnisotropicCudaCore::execute() {
    std::cout << "AnisotropicCudaCore--->gpuId: " <<gpuId<<"\n";
}
