//
// Created by student on 12/4/20.
//
#include "factory/executor/ElasticCpuCore.h"

factory::executor::ElasticCpuCore::ElasticCpuCore(int threads) :threads(threads){}

void factory::executor::ElasticCpuCore::execute() {
    std::cout << "ElasticCpuCore--->threads: " <<threads<<"\n";
}
