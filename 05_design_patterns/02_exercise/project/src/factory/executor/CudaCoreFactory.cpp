//
// Created by student on 12/4/20.
//
#include "factory/executor/CudaCoreFactory.h"
#include "factory/executor/AcousticCudaCore.h"
#include "factory/executor/AnisotropicCudaCore.h"
#include "factory/executor/ElasticCudaCore.h"
factory::executor::CudaCoreFactory::CudaCoreFactory(int gpuId) :gpuId(gpuId){}

std::shared_ptr<factory::executor::Core>factory::executor::CudaCoreFactory::create(std::string type) {
    if (type=="acoustic")
        return std::make_shared<AcousticCudaCore>(gpuId);
    if (type=="anisotropic")
        return std::make_shared<AnisotropicCudaCore>(gpuId);
    if (type=="elastic")
        return std::make_shared<ElasticCudaCore>(gpuId);
    throw std::runtime_error("CudaCore type is not defined: " + type);
}

