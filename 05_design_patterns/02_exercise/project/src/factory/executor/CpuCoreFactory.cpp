//
// Created by student on 12/4/20.
//
#include "factory/executor/CpuCoreFactory.h"
#include "factory/executor/AcousticCpuCore.h"
#include "factory/executor/AnisotropicCpuCore.h"
#include "factory/executor/ElasticCpuCore.h"
factory::executor::CpuCoreFactory::CpuCoreFactory(int threads) :threads(threads){}

std::shared_ptr<factory::executor::Core>factory::executor::CpuCoreFactory::create(std::string type) {
    if (type=="acoustic")
        return std::make_shared<AcousticCpuCore>(threads);
    if (type=="anisotropic")
        return std::make_shared<AnisotropicCpuCore>(threads);
    if (type=="elastic")
        return std::make_shared<ElasticCpuCore>(threads);
    throw std::runtime_error("CpuCore type is not defined: " + type);
}
