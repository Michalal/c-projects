//
// Created by student on 12/4/20.
//
#include "factory/executor/AnisotropicCpuCore.h"

factory::executor::AnisotropicCpuCore::AnisotropicCpuCore(int threads) :threads(threads){}

void factory::executor::AnisotropicCpuCore::execute() {
    std::cout << "AnisotropicCpuCore--->threads: " <<threads<<"\n";
}
