//
// Created by student on 12/4/20.
//
#include "factory/executor/ElasticCudaCore.h"

factory::executor::ElasticCudaCore::ElasticCudaCore(int gpuId) :gpuId(gpuId){}

void factory::executor::ElasticCudaCore::execute() {
    std::cout << "ElasticCudaCore--->gpuId: " <<gpuId<<"\n";
}
