//
// Created by student on 12/4/20.
//
#include "factory/executor/AcousticCpuCore.h"

factory::executor::AcousticCpuCore::AcousticCpuCore(int threads) :threads(threads){}

void factory::executor::AcousticCpuCore::execute() {
    std::cout << "AcousticCpuCore--->threads: " <<threads<<"\n";
}
