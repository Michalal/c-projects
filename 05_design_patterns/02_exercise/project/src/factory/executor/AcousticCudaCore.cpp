//
// Created by student on 12/4/20.
//
#include "factory/executor/AcousticCudaCore.h"

factory::executor::AcousticCudaCore::AcousticCudaCore(int gpuId) :gpuId(gpuId){}

void factory::executor::AcousticCudaCore::execute() {
    std::cout << "AcousticCudaCore--->gpuId: " <<gpuId<<"\n";
}
