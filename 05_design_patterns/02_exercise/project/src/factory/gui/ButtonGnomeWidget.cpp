//
// Created by student on 12/4/20.
//
#include "factory/gui/ButtonGnomeWidget.h"

factory::gui::ButtonGnomeWidget::ButtonGnomeWidget(int GnomeVersion) :GnomeVersion(GnomeVersion){}

void factory::gui::ButtonGnomeWidget::draw() {
    std::cout << "ButtonGnomeWidget--->GnomeVersion: " <<GnomeVersion<<"\n";
}
