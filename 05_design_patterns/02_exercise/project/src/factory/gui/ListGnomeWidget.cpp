//
// Created by student on 12/4/20.
//
#include "factory/gui/ListGnomeWidget.h"

factory::gui::ListGnomeWidget::ListGnomeWidget(int GnomeVersion) :GnomeVersion(GnomeVersion){}

void factory::gui::ListGnomeWidget::draw() {
    std::cout << "ListGnomeWidget--->GnomeVersion: " <<GnomeVersion<<"\n";
}
