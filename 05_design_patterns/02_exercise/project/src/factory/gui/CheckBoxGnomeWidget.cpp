//
// Created by student on 12/4/20.
//
#include "factory/gui/CheckBoxGnomeWidget.h"

factory::gui::CheckBoxGnomeWidget::CheckBoxGnomeWidget(int GnomeVersion) :GnomeVersion(GnomeVersion){}

void factory::gui::CheckBoxGnomeWidget::draw() {
    std::cout << "CheckBoxGnomeWidget--->GnomeVersion: " <<GnomeVersion<<"\n";
}
