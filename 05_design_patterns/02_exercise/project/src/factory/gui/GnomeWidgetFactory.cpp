//
// Created by student on 12/4/20.
//

#include "factory/gui/GnomeWidgetFactory.h"
#include "factory/gui/ButtonGnomeWidget.h"
#include "factory/gui/ListGnomeWidget.h"
#include "factory/gui/CheckBoxGnomeWidget.h"
factory::gui::GnomeWidgetFactory::GnomeWidgetFactory(int GnomeVersion) :GnomeVersion(GnomeVersion){}

std::shared_ptr<factory::gui::Widget>factory::gui::GnomeWidgetFactory::create(std::string type) {
    if (type=="button")
        return std::make_shared<ButtonGnomeWidget>(GnomeVersion);
    if (type=="list")
        return std::make_shared<ListGnomeWidget>(GnomeVersion);
    if (type=="check_box")
        return std::make_shared<CheckBoxGnomeWidget>(GnomeVersion);
    throw std::runtime_error("GnomeWidget type is not defined: " + type);
}