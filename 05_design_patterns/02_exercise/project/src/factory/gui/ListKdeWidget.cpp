//
// Created by student on 12/4/20.
//
#include "factory/gui/ListKdeWidget.h"

factory::gui::ListKdeWidget::ListKdeWidget(int KdeVersion) :KdeVersion(KdeVersion){}

void factory::gui::ListKdeWidget::draw() {
    std::cout << "ListKdeWidget--->KdeVersion: " <<KdeVersion<<"\n";
}
