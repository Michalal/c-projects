//
// Created by student on 12/4/20.
//
#include "factory/gui/KdeWidgetFactory.h"
#include "factory/gui/ButtonKdeWidget.h"
#include "factory/gui/ListKdeWidget.h"
#include "factory/gui/CheckBoxKdeWidget.h"
factory::gui::KdeWidgetFactory::KdeWidgetFactory(int KdeVersion) :KdeVersion(KdeVersion){}

std::shared_ptr<factory::gui::Widget>factory::gui::KdeWidgetFactory::create(std::string type) {
    if (type=="button")
        return std::make_shared<ButtonKdeWidget>(KdeVersion);
    if (type=="list")
        return std::make_shared<ListKdeWidget>(KdeVersion);
    if (type=="check_box")
        return std::make_shared<CheckBoxKdeWidget>(KdeVersion);
    throw std::runtime_error("KdeWidget type is not defined: " + type);
}