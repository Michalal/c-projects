//
// Created by student on 12/4/20.
//
#include "factory/gui/ButtonKdeWidget.h"

factory::gui::ButtonKdeWidget::ButtonKdeWidget(int KdeVersion) :KdeVersion(KdeVersion){}

void factory::gui::ButtonKdeWidget::draw() {
    std::cout << "ButtonKdeWidget--->KdeVersion: " <<KdeVersion<<"\n";
}