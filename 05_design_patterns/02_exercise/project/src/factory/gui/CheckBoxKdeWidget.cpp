//
// Created by student on 12/4/20.
//
#include "factory/gui/CheckBoxKdeWidget.h"

factory::gui::CheckBoxKdeWidget::CheckBoxKdeWidget(int KdeVersion) :KdeVersion(KdeVersion){}

void factory::gui::CheckBoxKdeWidget::draw() {
    std::cout << "CheckBoxKdeWidget--->KdeVersion: " <<KdeVersion<<"\n";
}
