//
// Created by student on 12/4/20.
//
#include "../../include/observer/Observable.h"
void::observer::Observable::registerObserver(Observer* a){
    observers.insert(a);
};
void::observer::Observable::unregisterObserver(Observer* a){
    observers.erase(a);
};
void::observer::Observable::notifyObservers(float value){
    for(Observer* observer : observers)
        observer->notify(value);
}