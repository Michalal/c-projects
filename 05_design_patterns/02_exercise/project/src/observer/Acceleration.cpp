//
// Created by student on 12/4/20.
//
#include "../../include/observer/Acceleration.h"
float::observer::Acceleration::get()
{
    if(hasFirstValue && hasSecondValue && hasThirdValue)
        return FirstValue+ThirdValue-2*SecondValue;
    return 0;
}

observer::Acceleration::Acceleration()
{
    hasFirstValue=false;
    hasSecondValue=false;
    hasThirdValue=false;
}

void::observer::Acceleration::notify(float a)
{
    if(!hasFirstValue)
    {
        FirstValue = a;
        hasFirstValue=true;
    }
    else if(!hasSecondValue)
    {
        SecondValue = a;
        hasSecondValue=true;
    }
    else if(!hasThirdValue)
    {
        ThirdValue=a;
        hasThirdValue=true;
    }
    else
    {
        FirstValue=SecondValue;
        SecondValue=ThirdValue;
        ThirdValue=a;
    }
}