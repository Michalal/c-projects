//
// Created by student on 12/4/20.
//

#ifndef PROJECT_CUDACOREFACTORY_H
#define PROJECT_CUDACOREFACTORY_H
#include "factory/executor/CoreFactory.h"
namespace factory{
    namespace executor{
        class CudaCoreFactory: public CoreFactory{
        public:
            explicit CudaCoreFactory(int gpuId);
            std::shared_ptr<Core> create(std::string type) override;

        private:
            int gpuId;
        };
    }
}
#endif //PROJECT_CUDACOREFACTORY_H
