//
// Created by student on 12/4/20.
//

#ifndef PROJECT_CPUCOREFACTORY_H
#define PROJECT_CPUCOREFACTORY_H
#include "factory/executor/CoreFactory.h"
namespace factory{
    namespace executor{
        class CpuCoreFactory: public CoreFactory{
        public:
            explicit CpuCoreFactory(int threads);
            std::shared_ptr<Core> create(std::string type) override;

        private:
            int threads;
        };
    }
}
#endif //PROJECT_CPUCOREFACTORY_H
