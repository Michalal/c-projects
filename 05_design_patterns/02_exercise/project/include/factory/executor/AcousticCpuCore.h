//
// Created by student on 12/4/20.
//

#ifndef PROJECT_ACOUSTICCPUCORE_H
#define PROJECT_ACOUSTICCPUCORE_H
#include "factory/executor/Core.h"
namespace factory{
    namespace executor{
        class AcousticCpuCore : public Core{
        public:
            explicit AcousticCpuCore(int threads);
            void execute() override;
        private:
            int threads;
        };
    }
}
#endif //PROJECT_ACOUSTICCPUCORE_H
