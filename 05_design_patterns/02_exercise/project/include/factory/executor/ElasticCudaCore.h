//
// Created by student on 12/4/20.
//

#ifndef PROJECT_ELASTICCUDACORE_H
#define PROJECT_ELASTICCUDACORE_H
#include "factory/executor/Core.h"
namespace factory{
    namespace executor{
        class ElasticCudaCore : public Core{
        public:
            explicit ElasticCudaCore(int gpuId);
            void execute() override;
        private:
            int gpuId;
        };
    }
}
#endif //PROJECT_ELASTICCUDACORE_H
