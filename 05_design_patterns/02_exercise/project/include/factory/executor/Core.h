//
// Created by student on 12/4/20.
//

#ifndef PROJECT_CORE_H
#define PROJECT_CORE_H
#include <iostream>
#include <memory>
namespace factory{
    namespace executor{
        class Core{
        public:
            virtual ~Core() = default;
            virtual void execute() = 0;
        };
    }
}
#endif //PROJECT_CORE_H
