//
// Created by student on 12/4/20.
//

#ifndef PROJECT_ELASTICCPUCORE_H
#define PROJECT_ELASTICCPUCORE_H
#include "factory/executor/Core.h"
namespace factory{
    namespace executor{
        class ElasticCpuCore : public Core{
        public:
            explicit ElasticCpuCore(int threads);
            void execute() override;
        private:
            int threads;
        };
    }
}
#endif //PROJECT_ELASTICCPUCORE_H
