//
// Created by student on 12/4/20.
//

#ifndef PROJECT_ANISOTROPICCUDACORE_H
#define PROJECT_ANISOTROPICCUDACORE_H
#include "factory/executor/Core.h"
namespace factory{
    namespace executor{
        class AnisotropicCudaCore : public Core{
        public:
            explicit AnisotropicCudaCore(int gpuId);
            void execute() override;
        private:
            int gpuId;
        };
    }
}
#endif //PROJECT_ANISOTROPICCUDACORE_H
