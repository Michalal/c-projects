//
// Created by student on 12/4/20.
//

#ifndef PROJECT_COREFACTORY_H
#define PROJECT_COREFACTORY_H
#include "Core.h"
namespace factory{
    namespace executor{
        class CoreFactory{
        public:
            virtual ~CoreFactory()=default;
            virtual std::shared_ptr<Core> create(std::string type)=0;
        };
    }
}
#endif //PROJECT_COREFACTORY_H
