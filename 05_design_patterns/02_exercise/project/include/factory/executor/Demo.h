//
// Created by student on 12/4/20.
//

#ifndef PROJECT_DEMO_H
#define PROJECT_DEMO_H
#include "CoreFactory.h"
namespace factory{
    namespace executor{
        class Demo{
        public:
            explicit Demo(std::shared_ptr<CoreFactory>factory);
            void run(std::string equation);

        private:
            std::shared_ptr<CoreFactory> factory;
        };
    }
}
#endif //PROJECT_DEMO_H
