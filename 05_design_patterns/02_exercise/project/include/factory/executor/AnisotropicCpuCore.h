//
// Created by student on 12/4/20.
//

#ifndef PROJECT_ANISOTROPICCPUCORE_H
#define PROJECT_ANISOTROPICCPUCORE_H
#include "factory/executor/Core.h"
namespace factory{
    namespace executor{
        class AnisotropicCpuCore : public Core{
        public:
            explicit AnisotropicCpuCore(int threads);
            void execute() override;
        private:
            int threads;
        };
    }
}
#endif //PROJECT_ANISOTROPICCPUCORE_H
