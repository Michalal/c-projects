//
// Created by student on 12/4/20.
//

#ifndef PROJECT_ACOUSTICCUDACORE_H
#define PROJECT_ACOUSTICCUDACORE_H
#include "factory/executor/Core.h"
namespace factory{
    namespace executor{
        class AcousticCudaCore : public Core{
        public:
            explicit AcousticCudaCore(int gpuId);
            void execute() override;
        private:
            int gpuId;
        };
    }
}
#endif //PROJECT_ACOUSTICCUDACORE_H
