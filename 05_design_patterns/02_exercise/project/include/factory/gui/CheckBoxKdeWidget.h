//
// Created by student on 12/4/20.
//

#ifndef PROJECT_CHECKBOXKDEWIDGET_H
#define PROJECT_CHECKBOXKDEWIDGET_H
#include "factory/gui/Widget.h"
namespace factory{
    namespace gui{
        class CheckBoxKdeWidget : public Widget{
        public:
            explicit CheckBoxKdeWidget(int KdeVersion);
            void draw() override;
        private:
            int KdeVersion;
        };
    }
}
#endif //PROJECT_CHECKBOXKDEWIDGET_H
