//
// Created by student on 12/4/20.
//

#ifndef PROJECT_GNOMEWIDGETFACTORY_H
#define PROJECT_GNOMEWIDGETFACTORY_H
#include "factory/gui/WidgetFactory.h"
namespace factory{
    namespace gui{
        class GnomeWidgetFactory: public WidgetFactory{
        public:
            explicit GnomeWidgetFactory(int GnomeVersion);
            std::shared_ptr<Widget> create(std::string type) override;

        private:
            int GnomeVersion;
        };
    }
}
#endif //PROJECT_GNOMEWIDGETFACTORY_H
