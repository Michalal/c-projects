//
// Created by student on 12/4/20.
//

#ifndef PROJECT_BUTTONGNOMEWIDGET_H
#define PROJECT_BUTTONGNOMEWIDGET_H
#include "factory/gui/Widget.h"
namespace factory{
    namespace gui{
        class ButtonGnomeWidget : public Widget{
        public:
            explicit ButtonGnomeWidget(int GnomeVersion);
            void draw() override;
        private:
            int GnomeVersion;
        };
    }
}
#endif //PROJECT_BUTTONGNOMEWIDGET_H
