//
// Created by student on 12/4/20.
//

#ifndef PROJECT_CHECKBOXGNOMEWIDGET_H
#define PROJECT_CHECKBOXGNOMEWIDGET_H
#include "factory/gui/Widget.h"
namespace factory{
    namespace gui{
        class CheckBoxGnomeWidget : public Widget{
        public:
            explicit CheckBoxGnomeWidget(int GnomeVersion);
            void draw() override;
        private:
            int GnomeVersion;
        };
    }
}
#endif //PROJECT_CHECKBOXGNOMEWIDGET_H
