//
// Created by student on 12/4/20.
//

#ifndef PROJECT_WIDGETFACTORY_H
#define PROJECT_WIDGETFACTORY_H
#include "Widget.h"
namespace factory{
    namespace gui{
        class WidgetFactory{
        public:
            virtual ~WidgetFactory()=default;
            virtual std::shared_ptr<Widget> create(std::string type)=0;
        };
    }
}
#endif //PROJECT_WIDGETFACTORY_H
