//
// Created by student on 12/4/20.
//

#ifndef PROJECT_LISTKDEWIDGET_H
#define PROJECT_LISTKDEWIDGET_H
#include "factory/gui/Widget.h"
namespace factory{
    namespace gui{
        class ListKdeWidget : public Widget{
        public:
            explicit ListKdeWidget(int KdeVersion);
            void draw() override;
        private:
            int KdeVersion;
        };
    }
}
#endif //PROJECT_LISTKDEWIDGET_H
