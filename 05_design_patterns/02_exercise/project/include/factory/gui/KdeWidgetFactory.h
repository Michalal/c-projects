//
// Created by student on 12/4/20.
//

#ifndef PROJECT_KDEWIDGETFACTORY_H
#define PROJECT_KDEWIDGETFACTORY_H

#include "factory/gui/WidgetFactory.h"
namespace factory{
    namespace gui{
        class KdeWidgetFactory: public WidgetFactory{
        public:
            explicit KdeWidgetFactory(int KdeVersion);
            std::shared_ptr<Widget> create(std::string type) override;

        private:
            int KdeVersion;
        };
    }
}
#endif //PROJECT_KDEWIDGETFACTORY_H
