//
// Created by student on 12/4/20.
//

#ifndef PROJECT_WIDGET_H
#define PROJECT_WIDGET_H
#include <iostream>
#include <memory>
namespace factory{
    namespace gui{
        class Widget{
            public:
            virtual ~Widget() = default;
            virtual void draw() = 0;
        };
    }
}
#endif //PROJECT_WIDGET_H
