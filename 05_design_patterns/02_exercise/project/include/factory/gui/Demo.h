//
// Created by student on 12/4/20.
//

#ifndef PROJECT_DEMO1_H \
#define PROJECT_DEMO1_H
#include "WidgetFactory.h"

namespace factory{
    namespace gui{
        class Demo{
        public:
            explicit Demo(std::shared_ptr<WidgetFactory>factory);
            void run(std::string type);

        private:
            std::shared_ptr<WidgetFactory> factory;
        };
    }
}
#endif //PROJECT_DEMO1_H
