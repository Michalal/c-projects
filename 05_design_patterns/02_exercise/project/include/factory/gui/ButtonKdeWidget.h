//
// Created by student on 12/4/20.
//

#ifndef PROJECT_BUTTONKDEWIDGET_H
#define PROJECT_BUTTONKDEWIDGET_H
#include "factory/gui/Widget.h"
namespace factory{
    namespace gui{
        class ButtonKdeWidget : public Widget{
        public:
            explicit ButtonKdeWidget(int KdeVersion);
            void draw() override;
        private:
            int KdeVersion;
        };
    }
}
#endif //PROJECT_BUTTONKDEWIDGET_H
