//
// Created by student on 12/4/20.
//

#ifndef PROJECT_LISTGNOMEWIDGET_H
#define PROJECT_LISTGNOMEWIDGET_H
#include "factory/gui/Widget.h"
namespace factory{
    namespace gui{
        class ListGnomeWidget : public Widget{
        public:
            explicit ListGnomeWidget(int GnomeVersion);
            void draw() override;
        private:
            int GnomeVersion;
        };
    }
}
#endif //PROJECT_LISTGNOMEWIDGET_H
