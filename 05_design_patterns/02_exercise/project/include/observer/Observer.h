//
// Created by student on 12/4/20.
//

#ifndef PROJECT_OBSERVER_H
#define PROJECT_OBSERVER_H
namespace observer{
    class Observer{
    public:
        virtual ~Observer()=default;
        virtual void notify(float)=0;
    };
}
#endif //PROJECT_OBSERVER_H
