//
// Created by student on 12/4/20.
//

#ifndef PROJECT_ACCELERATION_H
#define PROJECT_ACCELERATION_H
#include "observer/Observer.h"
namespace observer{
    class Acceleration : public Observer{
    public:
        float get();
        Acceleration();
        void notify(float);
    private:
        bool hasFirstValue;
        bool hasSecondValue;
        bool hasThirdValue;
        float FirstValue;
        float SecondValue;
        float ThirdValue;
    };
}
#endif //PROJECT_ACCELERATION_H
