//
// Created by student on 12/4/20.
//

#ifndef PROJECT_DISPLACEMENT_H
#define PROJECT_DISPLACEMENT_H
#include "observer/Observable.h"
namespace observer{
    class Displacement : public Observable{
    public:
        void set(float);
    private:
        float value;
    };
}
#endif //PROJECT_DISPLACEMENT_H
