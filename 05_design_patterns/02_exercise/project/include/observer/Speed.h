//
// Created by student on 12/4/20.
//

#ifndef PROJECT_SPEED_H
#define PROJECT_SPEED_H
#include "observer/Observer.h"
namespace observer{
    class Speed : public Observer{
    public:
        float get();
        Speed();
        void notify(float);
    private:
        bool hasFirstValue;
        bool hasSecondValue;
        float FirstValue;
        float SecondValue;
    };
}
#endif //PROJECT_SPEED_H
