#include "RingBuffer.h"

// TODO: ...
RingBuffer::RingBuffer(int k)
{
    size=k;
    beg=-1;
    end=-1;
    current_size=0;
}

int RingBuffer::Array_size()
{
    return size;
}
void RingBuffer::Add(int value)
{
    if(current_size<size)
        current_size++;
    end=(end+1)%size;
    Array[end]=value;
    if(beg==-1)
    {
        beg=0;
    }
    else if(end==beg)
    {
        beg=(beg+1)%size;
    }
}
int RingBuffer::Remove()
{
    int rt=Array[beg];
    current_size--;
    Array[beg]=0;
    beg=(beg+1)%size;
    return rt;
}