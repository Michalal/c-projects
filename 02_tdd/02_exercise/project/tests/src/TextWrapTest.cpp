#include "TextWrap.h"
#include <gtest/gtest.h>

TEST(TextWrap, Constructor) {

    TextWrap textWrap();
}
TEST(TextWrap, Constructor_1) {

    TextWrap textWrap(10);
}
TEST(TextWrap, Return_Number) {

    TextWrap textWrap(10);
    EXPECT_EQ(10,textWrap.Size());
}
TEST(TextWrap, First_String) {

    TextWrap textWrap(10);
    textWrap.Set_String("abc");
    EXPECT_EQ("abc",textWrap.wrap());
}
TEST(TextWrap, First_Split) {

    TextWrap textWrap(4);
    textWrap.Set_String("abcdef");
    EXPECT_EQ("abcd\nef",textWrap.wrap());
}
TEST(TextWrap, Split_Two_Words) {

    TextWrap textWrap(4);
    textWrap.Set_String("ab cdef");
    EXPECT_EQ("ab\ncdef",textWrap.wrap());
}
TEST(TextWrap, Split_With_Small_Size) {

    TextWrap textWrap(2);
    textWrap.Set_String("abc");
    EXPECT_EQ("ab\nc",textWrap.wrap());
}
TEST(TextWrap, Split_With_Small_Size1) {

    TextWrap textWrap(3);
    textWrap.Set_String("ab cde f g");
    EXPECT_EQ("ab\ncde\nf g",textWrap.wrap());
}
TEST(TextWrap, LoremIpsum_Test) {

    TextWrap textWrap(50);
    textWrap.Set_String("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis accumsan dignissim ante vel suscipit.");
    EXPECT_EQ("Lorem ipsum dolor sit amet, consectetur adipiscing\n"
              "elit. Duis accumsan dignissim ante vel suscipit.",textWrap.wrap());
}
TEST(TextWrap, LoremIpsum_Test50) {

    TextWrap textWrap(50);
    textWrap.Set_String("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis accumsan dignissim ante vel suscipit. Aenean suscipit ex porttitor, lobortis eros non, cursus nunc. In consectetur, magna nec sodales egestas, nisi felis tincidunt ipsum, et ornare massa magna sit amet eros. Etiam bibendum eros viverra augue ultrices vehicula. Etiam risus lectus, rhoncus vitae odio eget, sagittis malesuada nisl. Phasellus accumsan mi lorem, eget finibus purus pulvinar nec. Aliquam consequat ligula et maximus lobortis. In volutpat libero vitae eros gravida aliquet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce ac felis sapien. Nulla at lacus non risus imperdiet bibendum sit amet molestie purus. Nunc quis rutrum est. Sed id nisl non tortor facilisis rhoncus.");
    EXPECT_EQ("Lorem ipsum dolor sit amet, consectetur adipiscing\n"
              "elit. Duis accumsan dignissim ante vel suscipit.\n"
              "Aenean suscipit ex porttitor, lobortis eros non,\n"
              "cursus nunc. In consectetur, magna nec sodales\n"
              "egestas, nisi felis tincidunt ipsum, et ornare\n"
              "massa magna sit amet eros. Etiam bibendum eros\n"
              "viverra augue ultrices vehicula. Etiam risus\n"
              "lectus, rhoncus vitae odio eget, sagittis\n"
              "malesuada nisl. Phasellus accumsan mi lorem, eget\n"
              "finibus purus pulvinar nec. Aliquam consequat\n"
              "ligula et maximus lobortis. In volutpat libero\n"
              "vitae eros gravida aliquet. Interdum et malesuada\n"
              "fames ac ante ipsum primis in faucibus. Fusce ac\n"
              "felis sapien. Nulla at lacus non risus imperdiet\n"
              "bibendum sit amet molestie purus. Nunc quis rutrum\n"
              "est. Sed id nisl non tortor facilisis rhoncus.",textWrap.wrap());
}

TEST(TextWrap, LoremIpsum_Test80) {

    TextWrap textWrap(80);
    textWrap.Set_String("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis accumsan dignissim ante vel suscipit. Aenean suscipit ex porttitor, lobortis eros non, cursus nunc. In consectetur, magna nec sodales egestas, nisi felis tincidunt ipsum, et ornare massa magna sit amet eros. Etiam bibendum eros viverra augue ultrices vehicula. Etiam risus lectus, rhoncus vitae odio eget, sagittis malesuada nisl. Phasellus accumsan mi lorem, eget finibus purus pulvinar nec. Aliquam consequat ligula et maximus lobortis. In volutpat libero vitae eros gravida aliquet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce ac felis sapien. Nulla at lacus non risus imperdiet bibendum sit amet molestie purus. Nunc quis rutrum est. Sed id nisl non tortor facilisis rhoncus.");
    EXPECT_EQ(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis accumsan dignissim\n"
            "ante vel suscipit. Aenean suscipit ex porttitor, lobortis eros non, cursus nunc.\n"
            "In consectetur, magna nec sodales egestas, nisi felis tincidunt ipsum, et ornare\n"
            "massa magna sit amet eros. Etiam bibendum eros viverra augue ultrices vehicula.\n"
            "Etiam risus lectus, rhoncus vitae odio eget, sagittis malesuada nisl. Phasellus\n"
            "accumsan mi lorem, eget finibus purus pulvinar nec. Aliquam consequat ligula et\n"
            "maximus lobortis. In volutpat libero vitae eros gravida aliquet. Interdum et\n"
            "malesuada fames ac ante ipsum primis in faucibus. Fusce ac felis sapien. Nulla\n"
            "at lacus non risus imperdiet bibendum sit amet molestie purus. Nunc quis rutrum\n"
            "est. Sed id nisl non tortor facilisis rhoncus.",textWrap.wrap());
}
TEST(TextWrap, LoremIpsum_Test100) {

    TextWrap textWrap(100);
    textWrap.Set_String(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis accumsan dignissim ante vel suscipit. Aenean suscipit ex porttitor, lobortis eros non, cursus nunc. In consectetur, magna nec sodales egestas, nisi felis tincidunt ipsum, et ornare massa magna sit amet eros. Etiam bibendum eros viverra augue ultrices vehicula. Etiam risus lectus, rhoncus vitae odio eget, sagittis malesuada nisl. Phasellus accumsan mi lorem, eget finibus purus pulvinar nec. Aliquam consequat ligula et maximus lobortis. In volutpat libero vitae eros gravida aliquet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce ac felis sapien. Nulla at lacus non risus imperdiet bibendum sit amet molestie purus. Nunc quis rutrum est. Sed id nisl non tortor facilisis rhoncus.");
    EXPECT_EQ(
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis accumsan dignissim ante vel suscipit.\n"
            "Aenean suscipit ex porttitor, lobortis eros non, cursus nunc. In consectetur, magna nec sodales\n"
            "egestas, nisi felis tincidunt ipsum, et ornare massa magna sit amet eros. Etiam bibendum eros\n"
            "viverra augue ultrices vehicula. Etiam risus lectus, rhoncus vitae odio eget, sagittis malesuada\n"
            "nisl. Phasellus accumsan mi lorem, eget finibus purus pulvinar nec. Aliquam consequat ligula et\n"
            "maximus lobortis. In volutpat libero vitae eros gravida aliquet. Interdum et malesuada fames ac ante\n"
            "ipsum primis in faucibus. Fusce ac felis sapien. Nulla at lacus non risus imperdiet bibendum sit\n"
            "amet molestie purus. Nunc quis rutrum est. Sed id nisl non tortor facilisis rhoncus.", textWrap.wrap());
}
TEST(TextWrap, Last_Check_Test) {

    TextWrap textWrap(4);
    textWrap.Set_String("ab cdef klmnq");
    EXPECT_EQ("ab\ncdef\nklmn\nq", textWrap.wrap());
}