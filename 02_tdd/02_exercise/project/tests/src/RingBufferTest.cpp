#include "RingBuffer.h"
#include <gtest/gtest.h>

TEST(RingBuffer, Constructor) {

    RingBuffer ringBuffer{};
}
TEST(RingBuffer, Constructor_Array) {

    RingBuffer ringBuffer{5};
}
TEST(RingBuffer, Return_Size) {

    RingBuffer ringBuffer{5};
    EXPECT_EQ(5,ringBuffer.Array_size());
}
TEST(RingBuffer, Return_Size_8) {

    RingBuffer ringBuffer{8};
    EXPECT_EQ(8, ringBuffer.Array_size());
}

TEST(RingBuffer, Add_FirstValue_To_Array) {

    RingBuffer ringBuffer{8};
    ringBuffer.Add(3);
    EXPECT_EQ(3, ringBuffer.Array[0]);
}
TEST(RingBuffer, Add_SecondValue_To_Array) {

    RingBuffer ringBuffer{8};
    ringBuffer.Add(2);
    ringBuffer.Add(3);

    EXPECT_EQ(3, ringBuffer.Array[1]);
}
TEST(RingBuffer, Add_Third_Value_To_Array) {

    RingBuffer ringBuffer{8};
    ringBuffer.Add(2);
    ringBuffer.Add(3);
    ringBuffer.Add(4);
    EXPECT_EQ(2, ringBuffer.Array[0]);
    EXPECT_EQ(3, ringBuffer.Array[1]);
    EXPECT_EQ(4, ringBuffer.Array[2]);
}
TEST(RingBuffer, Small_Array) {

    RingBuffer ringBuffer{2};
    ringBuffer.Add(2);
    ringBuffer.Add(3);
    ringBuffer.Add(4);
    EXPECT_EQ(4, ringBuffer.Array[0]);
    EXPECT_EQ(3, ringBuffer.Array[1]);
}
TEST(RingBuffer, Test_Remove) {

    RingBuffer ringBuffer{5};
    ringBuffer.Add(2);
    ringBuffer.Add(3);
    ringBuffer.Add(4);
    ringBuffer.Remove();
    EXPECT_EQ(0, ringBuffer.Array[0]);
    EXPECT_EQ(3, ringBuffer.Array[1]);
}
    // TODO: ...
TEST(RingBuffer, Test_Remove_Small_Array) {

    RingBuffer ringBuffer{2};
    ringBuffer.Add(2);
    ringBuffer.Add(3);
    ringBuffer.Add(4);
    ringBuffer.Remove();
    EXPECT_EQ(4, ringBuffer.Array[0]);
    EXPECT_EQ(0, ringBuffer.Array[1]);
}
TEST(RingBuffer, Test_Size) {

    RingBuffer ringBuffer{3};
    ringBuffer.Add(2);
    ringBuffer.Add(3);
    ringBuffer.Add(4);
    EXPECT_EQ(3, ringBuffer.current_size);
}
TEST(RingBuffer, Test_Size_with_Remove) {

    RingBuffer ringBuffer{4};
    ringBuffer.Add(2);
    ringBuffer.Add(3);
    ringBuffer.Add(4);
    ringBuffer.Remove();
    EXPECT_EQ(2, ringBuffer.current_size);
}

TEST(RingBuffer, Test_Size_with_Remove_check_value) {

    RingBuffer ringBuffer{4};
    ringBuffer.Add(2);
    ringBuffer.Add(3);
    ringBuffer.Add(4);
    EXPECT_EQ(2,     ringBuffer.Remove());
    EXPECT_EQ(3,     ringBuffer.Remove());
    EXPECT_EQ(4,     ringBuffer.Remove());

}
TEST(RingBuffer, Example_from_Jupyter) {

    RingBuffer ringBuffer{5};
    EXPECT_EQ(0, ringBuffer.current_size);
    ringBuffer.Add(1);
    EXPECT_EQ(1, ringBuffer.current_size);
    ringBuffer.Add(2);
    EXPECT_EQ(2, ringBuffer.current_size);
    ringBuffer.Add(3);
    EXPECT_EQ(3, ringBuffer.current_size);
    EXPECT_EQ(1,ringBuffer.Remove());
    EXPECT_EQ(2, ringBuffer.current_size);
    ringBuffer.Add(4);
    EXPECT_EQ(3, ringBuffer.current_size);
    ringBuffer.Add(5);
    EXPECT_EQ(4, ringBuffer.current_size);
    ringBuffer.Add(6);
    EXPECT_EQ(5, ringBuffer.current_size);
    ringBuffer.Add(7);
    EXPECT_EQ(5, ringBuffer.current_size);
    EXPECT_EQ(3,ringBuffer.Remove());
    EXPECT_EQ(4, ringBuffer.current_size);
    EXPECT_EQ(4,ringBuffer.Remove());
    EXPECT_EQ(3, ringBuffer.current_size);

}