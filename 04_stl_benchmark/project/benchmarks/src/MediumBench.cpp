#include <list>
#include <unordered_map>
#include "Medium.h"
#include "BenchIncludes.h"

// TODO: Add benchmarks for operator<. operator==, and hash
void MediumOperator1(State& state) {

    Medium m1{};
    Medium m2{};
    m1.randomize();
    m2.randomize();
    for (auto _ : state)
    {
        m1.operator<(m2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumOperator1)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumOperatorSorted1(State& state) {
    Medium m1{};
    Medium m2{};
    m1.randomize();
    m2.randomize();
    std::sort(m1.data,m1.data+m1.SIZE);
    for (auto _ : state)
    {
        m1.operator<(m2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumOperatorSorted1)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumOperator2(State& state) {
    Medium m1{};
    Medium m2{};
    m1.randomize();
    m2.randomize();
    for (auto _ : state)
    {
        m1.operator==(m2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumOperator2)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumHash(State& state) {
    Medium m1{};
    m1.randomize();
    Medium m2{};
    m2.randomize();
    std::hash<Medium>hash;
    for (auto _ : state)
    {
        hash(m1);
        hash(m2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumHash)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListFront(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);

    for (auto _ : state)
    {
        auto p = list.front();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListFront)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListBack(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);

    for (auto _ : state)
    {
        auto p = list.back();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListBack)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListEmpty(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);

    for (auto _ : state)
    {
        auto p = list.empty();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListEmpty)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListSize(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    for (auto _ : state)
    {
        auto p = list.size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListMaxSize(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);

    for (auto _ : state)
    {
        auto p = list.max_size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListMaxSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListClear(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    for (auto _ : state)
    {
        std::list<Medium>list(size);
        list.clear();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListClear)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListInsert(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    std::list<Medium>::iterator it=list.begin();
    for (auto _ : state)
    {
        Medium s1{};
        it++;
        auto p=list.insert(it,s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListInsert)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListErase(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    for (auto _ : state)
    {
        Medium s1{};
        auto p=list.erase(list.begin());
        DoNotOptimize(p);
        ClobberMemory();
        list.insert(list.begin(),s1);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListErase)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListEraseForN(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    for (auto _ : state)
    {
        std::list<Medium>list(size);
        auto p=list.erase(list.begin(),list.end());
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListEraseForN)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListPush_Back(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    Medium s1{};
    for (auto _ : state)
    {
        list.push_back(s1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListPush_Back)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListPop_Back(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    Medium s1{};
    for (auto _ : state)
    {
        list.pop_back();
        ClobberMemory();
        list.push_front(s1);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListPop_Back)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListPush_Front(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    Medium s1{};
    for (auto _ : state)
    {
        list.push_front(s1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListPush_Front)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListPop_Front(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    Medium s1{};
    for (auto _ : state)
    {
        list.pop_front();
        ClobberMemory();
        list.push_back(s1);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListPop_Front)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListResize(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    auto size1=2*N;
    for (auto _ : state)
    {
        list.resize(size1);
        ClobberMemory();
        list.resize(size);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListResize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListSwap(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    std::list<Medium>list1(size);
    for (auto _ : state)
    {
        list.swap(list1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListSwap)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListMerge(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    for (auto _ : state)
    {
        std::list<Medium>list(size);
        std::list<Medium>list1(size);
        list.merge(list1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListMerge)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListSplice(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    std::list<Medium>list1(size);
    for (auto _ : state)
    {
        list.splice(list.begin(),list1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListSplice)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListRemove(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    Medium s1{};
    s1.randomize();
    for (auto _ : state)
    {
        list.remove(s1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListRemove)->RangeMultiplier(2)->Range(1u, 1u << 10u)->Complexity();

void MediumListRemoveIf(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    Medium s1{};
    for (auto _ : state)
    {
        list.remove_if([s1](Medium s2) {return s2<s1;});
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListRemoveIf)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListReverse(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    for (auto _ : state)
    {
        list.reverse();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListReverse)->RangeMultiplier(2)->Range(1u, 1u << 12u)->Complexity();

void MediumListUnique(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;

    for (auto _ : state)
    {
        std::list<Medium>list(size);
        list.unique();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListUnique)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumListSort(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Medium>list(size);
    for (auto _ : state)
    {
        list.sort();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumListSort)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();
// front, back, empty, size, max_size, clear, insert, erase, push_back, pop_back, push_front,
// pop_front, resize, swap, merge, splice, remove, remove_if, reverse, unique, sort

void MediumMultisetEmpty(State& state) {
    auto N = state.range(0);
    std::multiset<Medium>multiset;
    for (auto _ : state)
    {
        auto p = multiset.empty();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetEmpty)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumMultisetSize(State& state) {
    auto N = state.range(0);
    std::multiset<Medium>multiset;
    for (auto _ : state)
    {
        auto p = multiset.size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumMultisetMaxSize(State& state) {
    auto N = state.range(0);
    std::multiset<Medium>multiset;
    for (auto _ : state)
    {
        auto p = multiset.max_size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetMaxSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumMultisetClear(State& state) {
    auto N = state.range(0);
    for (auto _ : state)
    {
        state.PauseTiming();
        std::multiset<Medium>multiset;
        for(int i=0; i<N; i++)
        {
            Medium s1;
            multiset.insert(s1);
        }
        state.ResumeTiming();
        multiset.clear();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetClear)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumMultisetInsert(State& state) {
    auto N = state.range(0);
    std::multiset<Medium>multiset;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        s1.randomize();
        multiset.insert(s1);
    }
    for (auto _ : state)
    {
        Medium s1;
        s1.randomize();
        auto p = multiset.insert(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetInsert)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumMultisetInsertForN(State& state) {
    auto N = state.range(0);
    for (auto _ : state)
    {
        std::multiset<Medium>multiset;
        for(int i=0; i<N; i++)
        {
            Medium s1;
            s1.randomize();
            multiset.insert(s1);
        }
        Medium s1;
        s1.randomize();
        auto p = multiset.insert(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetInsertForN)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumMultisetErase(State& state) {
    auto N = state.range(0);
    std::multiset<Medium>multiset;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        s1.randomize();
        multiset.insert(s1);
    }
    Medium s1;
    s1.randomize();
    for (auto _ : state)
    {
        multiset.insert(s1);
        auto p = multiset.erase(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetErase)->RangeMultiplier(2)->Range(1u, 1u << 5u)->Complexity();

void MediumMultisetSwap(State& state) {
    auto N = state.range(0);
    std::multiset<Medium>multiset;
    std::multiset<Medium>multiset1;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        multiset.insert(s1);
        Medium s2;
        multiset1.insert(s2);
    }
    for (auto _ : state)
    {
        multiset.swap(multiset1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetSwap)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumMultisetCount(State& state) {
    auto N = state.range(0);
    std::multiset<Medium>multiset;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        s1.randomize();
        multiset.insert(s1);
    }
    Medium s1;
    s1.randomize();
    for (auto _ : state)
    {
        auto p = multiset.count(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetCount)->RangeMultiplier(2)->Range(1u, 1u << 5u)->Complexity();

void MediumMultisetFind(State& state) {
    auto N = state.range(0);
    std::multiset<Medium>multiset;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        s1.randomize();
        multiset.insert(s1);
    }
    Medium s1;
    s1.randomize();
    for (auto _ : state)
    {
        auto p = multiset.find(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetFind)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumMultisetEqualRange(State& state) {
    auto N = state.range(0);
    std::multiset<Medium>multiset;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        s1.randomize();
        multiset.insert(s1);
    }
    Medium s1;
    s1.randomize();
    for (auto _ : state)
    {
        auto p = multiset.equal_range(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetEqualRange)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumMultisetLowerBound(State& state) {
    auto N = state.range(0);
    std::multiset<Medium>multiset;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        s1.randomize();
        multiset.insert(s1);
    }
    Medium s1;
    s1.randomize();
    for (auto _ : state)
    {
        auto p = multiset.lower_bound(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetLowerBound)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumMultisetUpperBound(State& state) {
    auto N = state.range(0);
    std::multiset<Medium>multiset;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        s1.randomize();
        multiset.insert(s1);
    }
    Medium s1;
    s1.randomize();
    for (auto _ : state)
    {
        auto p = multiset.upper_bound(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumMultisetUpperBound)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();
// empty, size, max_size, clear, insert, erase, swap, count, find, equal_range, lower_bound, upper_bound

void MediumUnordered_mapEmpty(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for (auto _ : state)
    {
        auto p = unorderedMap.empty();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapEmpty)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumUnordered_mapSize(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for (auto _ : state)
    {
        auto p = unorderedMap.size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumUnordered_mapMaxSize(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for (auto _ : state)
    {
        auto p = unorderedMap.max_size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapMaxSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumUnordered_mapClear(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        Medium s2;
        s1.randomize();
        s2.randomize();
        unorderedMap.insert({s1,s2});
    }
    std::unordered_map<Medium,Medium>unorderedMap1=unorderedMap;
    for (auto _ : state)
    {
        unorderedMap.clear();
        ClobberMemory();
        unorderedMap=unorderedMap1;
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapClear)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void MediumUnordered_mapInsert(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        Medium s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Medium s1;
        Medium s2;
        auto p = unorderedMap.insert({s1,s2});
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapInsert)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumUnordered_mapErase(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        Medium s2;
        unorderedMap.insert({s1,s2});
    }
    std::unordered_map<Medium,Medium>unorderedMap1=unorderedMap;
    for (auto _ : state)
    {
        auto p = unorderedMap.erase(unorderedMap.begin());
        DoNotOptimize(p);
        ClobberMemory();
        unorderedMap=unorderedMap1;
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapErase)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumUnordered_mapSwap(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    std::unordered_map<Medium,Medium>unorderedMap1;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        Medium s2;
        unorderedMap.insert({s1,s2});
        unorderedMap1.insert({s2,s1});
    }
    for (auto _ : state)
    {
        unorderedMap.swap(unorderedMap1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapSwap)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumUnordered_mapAt(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        Medium s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Medium s1;
        Medium s2;
        auto p = (unorderedMap.at(s2)=s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapAt)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumUnordered_mapOperator(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        Medium s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Medium s1;
        Medium s2;
        auto p = (unorderedMap[s1]=s2);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapOperator)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumUnordered_mapCount(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        Medium s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Medium s1;
        auto p = unorderedMap.count(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapCount)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumUnordered_mapFind(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        Medium s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Medium s1;
        auto p = unorderedMap.find(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapFind)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumUnordered_mapEqualRange(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        Medium s2;
        s1.randomize();
        s2.randomize();
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Medium s1;
        s1.randomize();
        auto p = unorderedMap.equal_range(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapEqualRange)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MediumUnordered_mapRehash(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Medium s1;
        Medium s2;
        s1.randomize();
        s2.randomize();
        unorderedMap.insert({s1,s2});
    }
    std::unordered_map<Medium,Medium>unorderedMap1=unorderedMap;
    for (auto _ : state)
    {
        unorderedMap.rehash(N);
        ClobberMemory();
        unorderedMap=unorderedMap1;
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapRehash)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void MediumUnordered_mapReserve(State& state) {
    auto N = state.range(0);
    std::unordered_map<Medium,Medium>unorderedMap;
    for (auto _ : state)
    {
        unorderedMap.reserve(0);
        unorderedMap.reserve(N);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MediumUnordered_mapReserve)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();
//empty, size, max_size, clear, insert, erase, swap, at, operator[], count, find, equal_range, rehash, reserve