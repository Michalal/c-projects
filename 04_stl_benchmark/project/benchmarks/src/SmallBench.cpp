#include <list>
#include "Small.h"
#include "BenchIncludes.h"
#include <iostream>
#include <unordered_map>
// TODO: Add benchmarks for operator<. operator==, and hash

void SmallOperator1(State& state) {
    Small s1{};
    s1.randomize();
    Small s2{};
    s2.randomize();
    for (auto _ : state)
    {
        s1.operator<(s2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(SmallOperator1)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void SmallOperator2(State& state) {
    Small s1{};
    s1.randomize();
    Small s2{};
    s2.randomize();
    for (auto _ : state)
    {
        s1.operator==(s2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(SmallOperator2)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void SmallHash(State& state) {
    Small s1{};
    s1.randomize();
    Small s2{};
    s2.randomize();
    std::hash<Small>hash;
    for (auto _ : state)
    {
        hash(s1);
        hash(s2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(SmallHash)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListFront(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);

    for (auto _ : state)
    {
        auto p = list.front();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListFront)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListBack(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);

    for (auto _ : state)
    {
        auto p = list.back();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListBack)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListEmpty(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);

    for (auto _ : state)
    {
        auto p = list.empty();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListEmpty)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListSize(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    for (auto _ : state)
    {
        auto p = list.size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListMaxSize(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);

    for (auto _ : state)
    {
        auto p = list.max_size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListMaxSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListClear(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    for (auto _ : state)
    {
        std::list<Small>list(size);
        list.clear();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListClear)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListInsert(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    std::list<Small>::iterator it=list.begin();
    for (auto _ : state)
    {
        Small s1{};
        it++;
        auto p=list.insert(it,s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListInsert)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListErase(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    for (auto _ : state)
    {
        Small s1{};
        auto p=list.erase(list.begin());
        DoNotOptimize(p);
        ClobberMemory();
        list.insert(list.begin(),s1);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListErase)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListEraseForN(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    for (auto _ : state)
    {
        std::list<Small>list(size);
        auto p=list.erase(list.begin(),list.end());
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListEraseForN)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListPush_Back(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    Small s1{};
    for (auto _ : state)
    {
        list.push_back(s1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListPush_Back)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListPop_Back(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    Small s1{};
    for (auto _ : state)
    {
        list.pop_back();
        ClobberMemory();
        list.push_front(s1);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListPop_Back)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListPush_Front(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    Small s1{};
    for (auto _ : state)
    {
        list.push_front(s1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListPush_Front)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListPop_Front(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    Small s1{};
    for (auto _ : state)
    {
        list.pop_front();
        ClobberMemory();
        list.push_back(s1);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListPop_Front)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListResize(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    auto size1=2*N;
    for (auto _ : state)
    {
        list.resize(size1);
        ClobberMemory();
        list.resize(size);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListResize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListSwap(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    std::list<Small>list1(size);
    for (auto _ : state)
    {
        list.swap(list1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListSwap)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListMerge(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    for (auto _ : state)
    {
        std::list<Small>list(size);
        std::list<Small>list1(size);
        list.merge(list1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListMerge)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListSplice(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    std::list<Small>list1(size);
    for (auto _ : state)
    {
        list.splice(list.begin(),list1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListSplice)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListRemove(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    Small s1{};
    s1.randomize();
    for (auto _ : state)
    {
        list.remove(s1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListRemove)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListRemoveIf(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    Small s1{};
    for (auto _ : state)
    {
        list.remove_if([s1](Small s2) {return s2<s1;});
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListRemoveIf)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListReverse(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    for (auto _ : state)
    {
        list.reverse();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListReverse)->RangeMultiplier(2)->Range(5u, 5u << 20u)->Complexity();

void ListUnique(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;

    for (auto _ : state)
    {
        std::list<Small>list(size);
        list.unique();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListUnique)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void ListSort(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Small>list(size);
    for (auto _ : state)
    {
        list.sort();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(ListSort)->RangeMultiplier(2)->Range(1u, 1u << 20u)->Complexity();
 // front, back, empty, size, max_size, clear, insert, erase, push_back, pop_back, push_front,
// pop_front, resize, swap, merge, splice, remove, remove_if, reverse, unique, sort



void MultisetEmpty(State& state) {
    auto N = state.range(0);
    std::multiset<Small>multiset;
    for (auto _ : state)
    {
        auto p = multiset.empty();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetEmpty)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetSize(State& state) {
    auto N = state.range(0);
    std::multiset<Small>multiset;
    for (auto _ : state)
    {
        auto p = multiset.size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetMaxSize(State& state) {
    auto N = state.range(0);
    std::multiset<Small>multiset;
    for (auto _ : state)
    {
        auto p = multiset.max_size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetMaxSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetClear(State& state) {
    auto N = state.range(0);
    for (auto _ : state)
    {
        state.PauseTiming();
        std::multiset<Small>multiset;
        for(int i=0; i<N; i++)
        {
            Small s1;
            multiset.insert(s1);
        }
        state.ResumeTiming();
        multiset.clear();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetClear)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetInsert(State& state) {
    auto N = state.range(0);
    std::multiset<Small>multiset;
    for(int i=0; i<N; i++)
    {
        Small s1;
        s1.randomize();
        multiset.insert(s1);
    }
    for (auto _ : state)
    {
        Small s1;
        s1.randomize();
        auto p = multiset.insert(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetInsert)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetInsertForN(State& state) {
    auto N = state.range(0);
    for (auto _ : state)
    {
        std::multiset<Small>multiset;
        for(int i=0; i<N; i++)
        {
            Small s1;
            s1.randomize();
            multiset.insert(s1);
        }
        Small s1;
        s1.randomize();
        auto p = multiset.insert(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetInsertForN)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetErase(State& state) {
    auto N = state.range(0);
    std::multiset<Small>multiset;
    for(int i=0; i<N; i++)
    {
        Small s1;
        s1.randomize();
        multiset.insert(s1);
    }
    Small s1;
    for (auto _ : state)
    {
        auto p = multiset.erase(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetErase)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetSwap(State& state) {
    auto N = state.range(0);
    std::multiset<Small>multiset;
    std::multiset<Small>multiset1;
    for(int i=0; i<N; i++)
    {
        Small s1;
        multiset.insert(s1);
        Small s2;
        multiset1.insert(s2);
    }
    for (auto _ : state)
    {
        multiset.swap(multiset1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetSwap)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetCount(State& state) {
    auto N = state.range(0);
    std::multiset<Small>multiset;
    for(int i=0; i<N; i++)
    {
        Small s1;
        s1.randomize();
        multiset.insert(s1);
    }
    for (auto _ : state)
    {
        state.PauseTiming();
        Small s1;
        s1.randomize();
        state.ResumeTiming();
        auto p = multiset.count(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetCount)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetFind(State& state) {
    auto N = state.range(0);
    std::multiset<Small>multiset;
    for(int i=0; i<N; i++)
    {
        Small s1;
        s1.randomize();
        multiset.insert(s1);
    }
    for (auto _ : state)
    {
        Small s1;
        auto p = multiset.find(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetFind)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetEqualRange(State& state) {
    auto N = state.range(0);
    std::multiset<Small>multiset;
    for(int i=0; i<N; i++)
    {
        Small s1;
        s1.randomize();
        multiset.insert(s1);
    }
    for (auto _ : state)
    {
        Small s1;
        auto p = multiset.equal_range(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetEqualRange)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetLowerBound(State& state) {
    auto N = state.range(0);
    std::multiset<Small>multiset;
    for(int i=0; i<N; i++)
    {
        Small s1;
        s1.randomize();
        multiset.insert(s1);
    }
    for (auto _ : state)
    {
        Small s1;
        auto p = multiset.lower_bound(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetLowerBound)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void MultisetUpperBound(State& state) {
    auto N = state.range(0);
    std::multiset<Small>multiset;
    for(int i=0; i<N; i++)
    {
        Small s1;
        s1.randomize();
        multiset.insert(s1);
    }
    for (auto _ : state)
    {
        Small s1;
        auto p = multiset.upper_bound(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(MultisetUpperBound)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();
// empty, size, max_size, clear, insert, erase, swap, count, find, equal_range, lower_bound, upper_bound

void Unordered_mapEmpty(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for (auto _ : state)
    {
       auto p = unorderedMap.empty();
       DoNotOptimize(p);
       ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapEmpty)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void Unordered_mapSize(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for (auto _ : state)
    {
        auto p = unorderedMap.size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void Unordered_mapMaxSize(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for (auto _ : state)
    {
        auto p = unorderedMap.max_size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapMaxSize)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void Unordered_mapClear(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Small s1;
        Small s2;
        s1.randomize();
        s2.randomize();
        unorderedMap.insert({s1,s2});
    }
    std::unordered_map<Small,Small>unorderedMap1=unorderedMap;
    for (auto _ : state)
    {
        unorderedMap.clear();
        ClobberMemory();
        unorderedMap=unorderedMap1;
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapClear)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void Unordered_mapInsert(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Small s1;
        Small s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Small s1;
        Small s2;
        auto p = unorderedMap.insert({s1,s2});
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapInsert)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void Unordered_mapErase(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Small s1;
        Small s2;
        unorderedMap.insert({s1,s2});
    }
    std::unordered_map<Small,Small>unorderedMap1=unorderedMap;
    for (auto _ : state)
    {
        auto p = unorderedMap.erase(unorderedMap.begin());
        DoNotOptimize(p);
        ClobberMemory();
        unorderedMap=unorderedMap1;
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapErase)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void Unordered_mapSwap(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    std::unordered_map<Small,Small>unorderedMap1;
    for(int i=0; i<N; i++)
    {
        Small s1;
        Small s2;
        unorderedMap.insert({s1,s2});
        unorderedMap1.insert({s2,s1});
    }
    for (auto _ : state)
    {
        unorderedMap.swap(unorderedMap1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapSwap)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void Unordered_mapAt(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Small s1;
        Small s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Small s1;
        Small s2;
        auto p = (unorderedMap.at(s2)=s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapAt)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void Unordered_mapOperator(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Small s1;
        Small s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Small s1;
        Small s2;
        auto p = (unorderedMap[s1]=s2);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapOperator)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void Unordered_mapCount(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Small s1;
        Small s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Small s1;
        auto p = unorderedMap.count(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapCount)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void Unordered_mapFind(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Small s1;
        Small s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Small s1;
        auto p = unorderedMap.find(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapFind)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void Unordered_mapEqualRange(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Small s1;
        Small s2;
        s1.randomize();
        s2.randomize();
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Small s1;
        s1.randomize();
        auto p = unorderedMap.equal_range(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapEqualRange)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void Unordered_mapRehash(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Small s1;
        Small s2;
        s1.randomize();
        s2.randomize();
        unorderedMap.insert({s1,s2});
    }
    std::unordered_map<Small,Small>unorderedMap1=unorderedMap;
    for (auto _ : state)
    {
        unorderedMap.rehash(N);
        ClobberMemory();
        unorderedMap=unorderedMap1;
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapRehash)->RangeMultiplier(2)->Range(5u, 1u << 16u)->Complexity();

void Unordered_mapReserve(State& state) {
    auto N = state.range(0);
    std::unordered_map<Small,Small>unorderedMap;
    for (auto _ : state)
    {
        unorderedMap.reserve(0);
        unorderedMap.reserve(N);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(Unordered_mapReserve)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();
//empty, size, max_size, clear, insert, erase, swap, at, operator[], count, find, equal_range, rehash, reserve