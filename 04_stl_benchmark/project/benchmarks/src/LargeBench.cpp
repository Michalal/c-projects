#include <list>
#include <unordered_map>
#include "Large.h"
#include "BenchIncludes.h"

// TODO: Add benchmarks for operator<. operator==, and hash
void LargeOperator1(State& state) {

    Large l1{};
    Large l2{};
    l1.randomize();
    l2.randomize();
    for (auto _ : state)
    {
        l1.operator<(l2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeOperator1)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();
/*
void LargeOperatorSorted1(State& state) {
    Large l1{};
    Large l2{};
    l1.randomize();
    l2.randomize();
    std::sort(l1.data,l1.data+l1.SIZE);
    std::sort(l2.data,l2.data+l2.SIZE);
    for (auto _ : state)
    {
        l1.operator<(l2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeOperatorSorted1)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();
*/
void LargeOperator2(State& state) {

    Large l1{};
    Large l2{};
    l1.randomize();
    l2.randomize();
    for (auto _ : state)
    {
        l1.operator==(l2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeOperator2)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();

void LargeHash(State& state) {

    Large l1{};
    Large l2{};
    l1.randomize();
    l2.randomize();
    std::hash<Large>hash;
    for (auto _ : state)
    {
        hash(l1);
        hash(l2);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeHash)->RangeMultiplier(2)->Range(1u, 1u << 15u)->Complexity();


void LargeListFront(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);

    for (auto _ : state)
    {
        auto p = list.front();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListFront)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListBack(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);

    for (auto _ : state)
    {
        auto p = list.back();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListBack)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListEmpty(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);

    for (auto _ : state)
    {
        auto p = list.empty();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListEmpty)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListSize(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    for (auto _ : state)
    {
        auto p = list.size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListSize)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListMaxSize(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);

    for (auto _ : state)
    {
        auto p = list.max_size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListMaxSize)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListClear(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    for (auto _ : state)
    {
        std::list<Large>list(size);
        list.clear();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListClear)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListInsert(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    std::list<Large>::iterator it=list.begin();
    for (auto _ : state)
    {
        Large s1{};
        it++;
        auto p=list.insert(it,s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListInsert)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListErase(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    for (auto _ : state)
    {
        Large s1{};
        auto p=list.erase(list.begin());
        DoNotOptimize(p);
        ClobberMemory();
        list.insert(list.begin(),s1);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListErase)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListEraseForN(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    for (auto _ : state)
    {
        std::list<Large>list(size);
        auto p=list.erase(list.begin(),list.end());
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListEraseForN)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListPush_Back(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    Large s1{};
    for (auto _ : state)
    {
        list.push_back(s1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListPush_Back)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListPop_Back(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    Large s1{};
    for (auto _ : state)
    {
        list.pop_back();
        ClobberMemory();
        list.push_front(s1);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListPop_Back)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListPush_Front(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    Large s1{};
    for (auto _ : state)
    {
        list.push_front(s1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListPush_Front)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListPop_Front(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    Large s1{};
    for (auto _ : state)
    {
        list.pop_front();
        ClobberMemory();
        list.push_back(s1);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListPop_Front)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListResize(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    auto size1=2*N;
    for (auto _ : state)
    {
        list.resize(size1);
        ClobberMemory();
        list.resize(size);
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListResize)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListSwap(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    std::list<Large>list1(size);
    for (auto _ : state)
    {
        list.swap(list1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListSwap)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListMerge(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    for (auto _ : state)
    {
        std::list<Large>list(size);
        std::list<Large>list1(size);
        list.merge(list1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListMerge)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListSplice(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    std::list<Large>list1(size);
    for (auto _ : state)
    {
        list.splice(list.begin(),list1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListSplice)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListRemove(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    Large s1{};
    s1.randomize();
    for (auto _ : state)
    {
        list.remove(s1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListRemove)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListRemoveIf(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    Large s1{};
    for (auto _ : state)
    {
        list.remove_if([s1](Large s2) {return s2<s1;});
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListRemoveIf)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListReverse(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    for (auto _ : state)
    {
        list.reverse();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListReverse)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListUnique(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;

    for (auto _ : state)
    {
        std::list<Large>list(size);
        list.unique();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListUnique)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeListSort(State& state) {
    auto N = state.range(0);
    auto size = (std::size_t)N;
    std::list<Large>list(size);
    for (auto _ : state)
    {
        list.sort();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeListSort)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();
// front, back, empty, size, max_size, clear, insert, erase, push_back, pop_back, push_front,
// pop_front, resize, swap, merge, splice, remove, remove_if, reverse, unique, sort
void LargeMultisetEmpty(State& state) {
    auto N = state.range(0);
    std::multiset<Large>multiset;
    for (auto _ : state)
    {
        auto p = multiset.empty();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetEmpty)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetSize(State& state) {
    auto N = state.range(0);
    std::multiset<Large>multiset;
    for (auto _ : state)
    {
        auto p = multiset.size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetSize)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetMaxSize(State& state) {
    auto N = state.range(0);
    std::multiset<Large>multiset;
    for (auto _ : state)
    {
        auto p = multiset.max_size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetMaxSize)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetClear(State& state) {
    auto N = state.range(0);
    for (auto _ : state)
    {
        state.PauseTiming();
        std::multiset<Large>multiset;
        for(int i=0; i<N; i++)
        {
            Large s1;
            multiset.insert(s1);
        }
        state.ResumeTiming();
        multiset.clear();
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetClear)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetInsert(State& state) {
    auto N = state.range(0);
    std::multiset<Large>multiset;
    for(int i=0; i<N; i++)
    {
        Large s1;
        s1.randomize();
        multiset.insert(s1);
    }
    for (auto _ : state)
    {
        Large s1;
        s1.randomize();
        auto p = multiset.insert(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetInsert)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetInsertForN(State& state) {
    auto N = state.range(0);
    for (auto _ : state)
    {
        std::multiset<Large>multiset;
        for(int i=0; i<N; i++)
        {
            Large s1;
            s1.randomize();
            multiset.insert(s1);
        }
        Large s1;
        s1.randomize();
        auto p = multiset.insert(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetInsertForN)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetErase(State& state) {
    auto N = state.range(0);
    std::multiset<Large>multiset;
    for(int i=0; i<N; i++)
    {
        Large s1;
        s1.randomize();
        multiset.insert(s1);
    }
    Large s1;
    s1.randomize();
    for (auto _ : state)
    {
        auto p = multiset.erase(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetErase)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetSwap(State& state) {
    auto N = state.range(0);
    std::multiset<Large>multiset;
    std::multiset<Large>multiset1;
    for(int i=0; i<N; i++)
    {
        Large s1;
        multiset.insert(s1);
        Large s2;
        multiset1.insert(s2);
    }
    for (auto _ : state)
    {
        multiset.swap(multiset1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetSwap)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetCount(State& state) {
    auto N = state.range(0);
    std::multiset<Large>multiset;
    for(int i=0; i<N; i++)
    {
        Large s1;
        s1.randomize();
        multiset.insert(s1);
    }
    for (auto _ : state)
    {
        //state.PauseTiming();
        Large s1;
        s1.randomize();
       // state.ResumeTiming();
        auto p = multiset.count(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetCount)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetFind(State& state) {
    auto N = state.range(0);
    std::multiset<Large>multiset;
    for(int i=0; i<N; i++)
    {
        Large s1;
        s1.randomize();
        multiset.insert(s1);
    }

    Large s1;
    s1.randomize();
    for (auto _ : state)
    {
        multiset.insert(s1);
        auto p = multiset.find(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetFind)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetEqualRange(State& state) {
    auto N = state.range(0);
    std::multiset<Large>multiset;
    for(int i=0; i<N; i++)
    {
        Large s1;
        s1.randomize();
        multiset.insert(s1);
    }

    Large s1;
    s1.randomize();
    for (auto _ : state)
    {
        auto p = multiset.equal_range(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetEqualRange)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetLowerBound(State& state) {
    auto N = state.range(0);
    std::multiset<Large>multiset;
    for(int i=0; i<N; i++)
    {
        Large s1;
        s1.randomize();
        multiset.insert(s1);
    }

    Large s1;
    s1.randomize();
    for (auto _ : state)
    {
        auto p = multiset.lower_bound(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetLowerBound)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeMultisetUpperBound(State& state) {
    auto N = state.range(0);
    std::multiset<Large>multiset;
    for(int i=0; i<N; i++)
    {
        Large s1;
        s1.randomize();
        multiset.insert(s1);
    }

    Large s1;
    s1.randomize();
    for (auto _ : state)
    {
        auto p = multiset.upper_bound(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeMultisetUpperBound)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();
// empty, size, max_size, clear, insert, erase, swap, count, find, equal_range, lower_bound, upper_bound

void LargeUnordered_mapEmpty(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for (auto _ : state)
    {
        auto p = unorderedMap.empty();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapEmpty)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapSize(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for (auto _ : state)
    {
        auto p = unorderedMap.size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapSize)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapMaxSize(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for (auto _ : state)
    {
        auto p = unorderedMap.max_size();
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapMaxSize)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapClear(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Large s1;
        Large s2;
        s1.randomize();
        s2.randomize();
        unorderedMap.insert({s1,s2});
    }
    std::unordered_map<Large,Large>unorderedMap1=unorderedMap;
    for (auto _ : state)
    {
        unorderedMap.clear();
        ClobberMemory();
        unorderedMap=unorderedMap1;
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapClear)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapInsert(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Large s1;
        Large s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Large s1;
        Large s2;
        auto p = unorderedMap.insert({s1,s2});
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapInsert)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapErase(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Large s1;
        Large s2;
        unorderedMap.insert({s1,s2});
    }
    std::unordered_map<Large,Large>unorderedMap1=unorderedMap;
    for (auto _ : state)
    {
        auto p = unorderedMap.erase(unorderedMap.begin());
        DoNotOptimize(p);
        ClobberMemory();
        unorderedMap=unorderedMap1;
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapErase)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapSwap(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    std::unordered_map<Large,Large>unorderedMap1;
    for(int i=0; i<N; i++)
    {
        Large s1;
        Large s2;
        unorderedMap.insert({s1,s2});
        unorderedMap1.insert({s2,s1});
    }
    for (auto _ : state)
    {
        unorderedMap.swap(unorderedMap1);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapSwap)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapAt(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Large s1;
        Large s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Large s1;
        Large s2;
        auto p = (unorderedMap.at(s2)=s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapAt)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapOperator(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Large s1;
        Large s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Large s1;
        Large s2;
        auto p = (unorderedMap[s1]=s2);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapOperator)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapCount(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Large s1;
        Large s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Large s1;
        auto p = unorderedMap.count(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapCount)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapFind(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Large s1;
        Large s2;
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Large s1;
        auto p = unorderedMap.find(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapFind)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapEqualRange(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Large s1;
        Large s2;
        s1.randomize();
        s2.randomize();
        unorderedMap.insert({s1,s2});
    }
    for (auto _ : state)
    {
        Large s1;
        s1.randomize();
        auto p = unorderedMap.equal_range(s1);
        DoNotOptimize(p);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapEqualRange)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapRehash(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for(int i=0; i<N; i++)
    {
        Large s1;
        Large s2;
        s1.randomize();
        s2.randomize();
        unorderedMap.insert({s1,s2});
    }
    std::unordered_map<Large,Large>unorderedMap1=unorderedMap;
    for (auto _ : state)
    {
        unorderedMap.rehash(N);
        ClobberMemory();
        unorderedMap=unorderedMap1;
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapRehash)->RangeMultiplier(2)->Range(1u, 1u << 8u)->Complexity();

void LargeUnordered_mapReserve(State& state) {
    auto N = state.range(0);
    std::unordered_map<Large,Large>unorderedMap;
    for (auto _ : state)
    {
        unorderedMap.reserve(0);
        unorderedMap.reserve(N);
        ClobberMemory();
    }
    state.SetComplexityN(state.range(0));
}

BENCHMARK(LargeUnordered_mapReserve)->RangeMultiplier(2)->Range(1u, 1u << 12u)->Complexity();
//empty, size, max_size, clear, insert, erase, swap, at, operator[], count, find, equal_range, rehash, reserve