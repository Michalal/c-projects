#include "TestIncludes.h"

#include <vector>
#include <array>
#include <deque>
#include <list>
#include <forward_list>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>

#include "Large.h"

TEST(LargeTest, SizeIsOneMegabyte) {

    EXPECT_EQ(1024u*1024u, sizeof(Large));
}

TEST(LargeTest, CreateObject) {

    Large large{};
}

TEST(LargeTest, HasLessThenOperator) {

    Large a{};
    Large b{};

    a < b;
}

TEST(LargeTest, HasEqualityOperator) {

    Large a{};
    Large b{};

    a == b;
}

TEST(LargeTest, CanBeHashed) {

    Large large{};
    std::hash<Large> hash;

    hash(large);
}

TEST(LargeTest, Collections) {

    Large large{};

    std::vector<Large> vector{};
    vector.push_back(large);

    std::array<Large, 1> array{};
    array[0] = large;

    std::deque<Large> deque{};
    deque.push_back(large);

    std::list<Large> list{};
    list.push_back(large);

    std::forward_list<Large> forward_list{};
    forward_list.push_front(large);

    std::map<Large, bool> map{};
    map[large] = true;

    std::set<Large> set{};
    set.insert(large);

    std::unordered_map<Large, bool> unordered_map{};
    unordered_map[large] = true;

    std::unordered_set<Large> unordered_set{};
    unordered_set.insert(large);
}

TEST(LargeTest, Randomize) {

    Large large{};
    large.randomize();

    auto count = 0u;

    for (double i : large.data) {

        ASSERT_LE(0.0, i);
        ASSERT_GE(1.0, i);

        if (i != 0.0)
            ++count;
    }

    EXPECT_NE(0u, count) << "All elements were zero?";
}

TEST(LargeTest, Clear) {

    Large large{};
    large.randomize();
    large.clear();

    for (double i : large.data) {
        ASSERT_DOUBLE_EQ(0.0, i);
    }
}


// TODO: Add tests for your operators implementation!
TEST(LargeTest, LargeOperator) {

    Large large_1{};
    Large large_2{};
    for(int a=0; a<5; a++)
    {
        large_1.data[a]=a*21.37;
        large_2.data[a]=a*21.37;
    }
    large_1.data[2]=21;
    large_2.data[2]=37;
    EXPECT_EQ(true,large_1<large_2);
}

TEST(LargeTest, LargeOperator1) {

    Large large_1{};
    Large large_2{};
    for(int a=0; a<5; a++)
    {
        large_1.data[a]=a*21.37;
        large_2.data[a]=a*21.37;
    }
    large_1.data[5]=210.3;
    large_2.data[5]=210;
    EXPECT_EQ(false,large_1<large_2);
}

TEST(LargeTest, LargeOperator2) {

    Large large_1{};
    Large large_2{};
    for(int a=0; a<5; a++)
    {
        large_1.data[a]=a*21.37;
        large_2.data[a]=a*21.37;
    }
    EXPECT_EQ(false,large_1<large_2);
}

TEST(LargeTest, LargeEqualOperator) {

    Large large_1{};
    Large large_2{};
    for(int a=0; a<5; a++)
    {
        large_1.data[a]=a*21.37;
        large_2.data[a]=a*21.37;
    }
    EXPECT_EQ(true,large_1==large_2);
}

TEST(LargeTest, LargeEqualOperator1) {

    Large large_1{};
    Large large_2{};
    for(int a=0; a<5; a++)
    {
        large_1.data[a]=a*21.37;
        large_2.data[a]=a*21.37;
    }
    large_1.data[5]=21.3337;
    large_1.data[5]=21.3338;
    EXPECT_EQ(false,large_1==large_2);
}

TEST(LargeTest, LargeHash) {

    Large large_1{};
    Large large_2{};
    for(int a=0; a<5; a++)
    {
        large_1.data[a]=a*21.37;
        large_2.data[a]=a*21.37;
    }
    std::hash <Large> hash;
    EXPECT_EQ(hash(large_1), hash(large_2));
}

TEST(LargeTest, LargeHash2) {

    Large large_1{};
    Large large_2{};
    for(int a=0; a<5; a++)
    {
        large_1.data[a]=a*21.37;
        large_2.data[a]=a*21.37;
    }
    large_1.data[5]=21.3337;
    large_1.data[5]=21.3338;
    std::hash <Large> hash;
    EXPECT_NE(hash(large_1), hash(large_2));
}