#pragma once

#include <random>

struct Medium {

    constexpr static unsigned SIZE = 256u;
    int data[SIZE];

    void clear() {
        std::fill_n(data, SIZE, 0);
    }

    void randomize() {
        static std::random_device rd{};
        static std::mt19937 gen{rd()};
        static std::uniform_int_distribution<int> dis{};

        for (int &i : data)
            i = dis(gen);
    }

    bool operator<(const Medium &rhs) const {

        // TODO: Implement me!
        int s=0;
        for(int i=0; i<SIZE; i++) {
            if (this->data[i] > rhs.data[i])
                return false;
            else if (this->data[i] == rhs.data[i])
                s++;
        }
        if(s==SIZE)
            return false;
        return true;
    }

    bool operator==(const Medium &rhs) const {

        // TODO: Implement me!
        for(int i=0; i<SIZE; i++)
            if(this->data[i]!=rhs.data[i])
                return false;
        return true;
    }
};

namespace std {
    template<>
    struct hash<Medium> {
        std::size_t operator()(const Medium &d) const {

            // TODO: Implement me!
            int y=0, p=2137, q=21372137;
            for(int i=0; i<d.SIZE; i++) {
                y=(y*p)+d.data[i];
                if(y>=q) {
                    y%=q;
                }
            }
            return hash<int>()(y);
        }
    };
}