# STL Benchmarks - Report

TODO: ...

ZAD 1

A. Przejsc po wszystkich wartosciach (np petla for lub while), bo inaczej porowna nam tylko pierwsze wartosci. Jesli na jakiejs pozycji elementy sie roznia lub przynajmniej jeden jest pusty, to zwracamy ten z mniejszym elementem lub ktory jest pusty (true dla pierwszego, false dla drugiego), a jak sa rowne, to kontynuujemy dla nastepnych elementow. 

B. Petla, ktora przechodzi po blokach danych i zwraca false od razu jesli natrafi na roznice (nie ma sensu isc dalej, bo i tak sa rozne). Natomiast jesli petla przejdzie po wszystkich wartosciach, to zwraca true

C. Celem jest wyznaczenie jednoznacznego klucza dla jakies wartosci, ktora wstawiamy do struktury. Matematycznie to powinna być prawdziwa funkcja (czyli dla danego argumentu zawsze zwraca te sama wartosc), chcemy rowniez, zeby mozliwie dobrze rozrzucala wartosci (czyli zeby kolizje byly jak najrzadziej)

D. Przechodze petla for i porownuje kolejne wartosci w przypadku operatorow, jesli wartosci sa rozne, to zwracam odpowiedni werdykt, jesli dojde do konca to sa rowne. 
Stosuje haszowanie wielomianowe dla liczby pierwszej i moduluje przez druga wieksza liczbe pierwsza (aby nie przejsc poza zakres int). Ta funkcja nie jest latwa do odwrocenia i dosc dobrze rozrzuca wartosci.



ZAD 2.

A.Operator< testy sprawdzajace wrzucanie dla trzech przypadkow (dwie kombinacje rozne w obie strony i rowne)

Operator== testy dla tej samej tabeli i roznej tabeli

Hash testy sprawdzajace czy wartosci hash maja ta sama wartosc czy rozna w zaleznosci od tablic

Dla wszystkich testow wyniki sie zgadzaja

B. Powinnismy sami  wybierac liczby, bo wtedy jestesmy w stanie znac poprawny wynik i sprawdzic czy jest on taki jak oczekiwany rezultat. Oprocz tego w przypadku losowego dobierania liczb nie wiemy ktory obiekt jest mniejszy wiec nie mamy jak porownac wyniki (chyba ze przejdziemy debuggerem) 


ZAD 3.

A. Wszystkie testy na benchmark mialy ta sama strukture, czyli ze tworzylem dwa obiekty (uzywajac randomize) i uruchamialem odpowiednia funkcje. 
Dla kazdego rodzaju funkcji czas dzialania benchmarkow byl porownywalny niezaleznie od ich ilosci, ktore w moim przypadku byly kolejnymi potegami 2

B. Bedzie dluzszy czas wykonania (w niektorych przypadkach moze nawet sie nie wykonac)

C. Bedzie dluzszy czas wykonania 

D. Bedzie dluzszy czas wykonania 

E. Wyniki praktycznie sie nie roznia (zlozonosc okolo O(1)). Oznacza to ze zawsze przechodzi mniej wiecej po tej samej liczbie elementow, co sie zgadza z tym bo zawsze mamy tablice o rozmiarze SIZE. Dla funkcji Hash trzeba zawsze przejsc po calej dlugosci co powoduje ze czas wykonania hash jest dlyzszy niz operatorow gdzie przy ranomowych danych latwo mozna skonczyc porownanie juz po kilku krokach
Przykladowe wyniki Operator1 to <, Opertaor2 to ==

SmallOperator1/32768              7.76 ns         7.38 ns     86009709
SmallOperator1_BigO               7.72 (1)        7.56 (1)
SmallOperator2/32768              8.58 ns         8.01 ns     91300740
SmallOperator2_BigO               8.13 (1)        7.95 (1) 
SmallHash/32768                   23.7 ns         23.7 ns     29666174
SmallHash_BigO                   24.85 (1)       24.06 (1) 

MediumOperator1/32768             10.8 ns         10.7 ns     74419621
MediumOperator1_BigO             11.80 (1)       11.49 (1)
MediumOperator2/32768             9.61 ns         9.59 ns     72285065
MediumOperator2_BigO              9.82 (1)        9.75 (1)
MediumHash/32768                  4390 ns         4131 ns       157065
MediumHash_BigO                4176.16 (1)     4065.30 (1) 

LargeOperator1/32768              8.84 ns         8.79 ns     58939794
LargeOperator1_BigO              11.83 (1)       11.69 (1)  
LargeOperator2/32768              9.51 ns         9.45 ns     75526585
LargeOperator2_BigO              10.39 (1)        9.94 (1)  
LargeHash/32768                2123118 ns      2121437 ns          323
LargeHash_BigO              2170306.27 (1)  2150272.09 (1)  


F. Ogolnie lepiej jest zainicjalizowac randomowymi danymi, gdyz wtedy jestesmy w stanie zobaczyc roznice w czasie dzialania dla roznych danych, bo jesli bysmy mieli ustalone wartosci, to nie byloby odchylenia w czasie



ZAD 4. 


A. Struktura do tworzenia benchmarkow do mierzenia zlozonosci Big(O) rozni sie przede wszystkim zakresem danych dla ktorych beda wykonywane testy. 
Korzysta się rowniez z funkcji Complexity(), ktora na podstawie danych zasugeruje nam zlozonosc dzialania wraz z ewentualnym bledem.

C. Wnioski z trybu Debug Small: 

List:

 front O(1) -tak samo jak na cppreference,
 back O(1) -tak samo jak na cppreference,
 empty O(1) -tak samo jak na cppreference,
 size O(1) -tak samo jak na cppreference,
 max_size O(1) -tak samo jak na cppreference,
 clear O(N) -tak samo jak na cppreference,
 insert O(1) -tak samo jak na cppreference (wstawiam jeden element),
 erase O(1) -tak samo jak na cppreference (zrobilem tez benchmark jak jest erase calej listy zeby sprawdzic czy bedzie wtedy bedzie zlozonosc liniowa),
 push_back O(1) -tak samo jak na cppreference,
 pop_back O(1) -tak samo jak na cppreference,
 push_front O(1) -tak samo jak na cppreference,
 pop_front O(1) -tak samo jak na cppreference,
 resize O(roznica rozmiaru) -tak samo jak na cppreference, bo u mnie za kazdym razem rozmiar jest zwiekszony dwukrtonie,czyli powinna byc zlozonosc O(N),
 swap O(1) -tak samo jak na cppreference,
 merge O(rozmiar obu tablic) -tak samo jak na cppreference, bo za kazdym razem daje merge tablicy o rozmiarze n, wiec zlozonosc powinna byc O(N),
 splice O(1) -tak samo jak na cppreference,
 remove O(N) -tak samo jak na cppreference,
 remove_if O(N) -tak samo jak na cppreference,
 reverse O(N) -tak samo jak na cppreference (zmienilem ilosc benchmarkow),
 unique O(N) -tak samo jak na cppreference,
 sort O(NlogN) -tak samo jak na cppreference,
 
Multiset:

 empty O(1) -tak samo jak na cppreference,
 size O(1) -tak samo jak na cppreference,
 max_size O(1) -tak samo jak na cppreference,
 clear O(N) -tak samo jak na cppreference,
 insert O(1) -inaczej niz na cppreference. Roznica pojawia sie dlatego ze zlonosc na cppreference O(logN) to zlozonosc pesymistyczna. U mnie widac odchylenia od O(1) co swiadczy ze sa rozne dane przez co znajdowanie miejsca do wstawinei roznie trwa. Z ciekawosci zrobilem sobie benchmark dla N przypadkow i zlozonosc wyszla NlogN (N z powodu ilosci elementow i logN z powodu insert),
 erase  O(logN) -tak samo jak na cppreference, 
 swap  O(1) -tak samo jak na cppreference,
 count O(logN) -tak samo jak na cppreference, 
 find O(logN) -tak samo jak na cppreference, 
 equal_range O(logN) -tak samo jak na cppreference, 
 lower_bound O(logN) -tak samo jak na cppreference, 
 upper_bound O(logN) -tak samo jak na cppreference,
  
Unordered_map:

 empty O(1) -tak samo jak na cppreference,
 size O(1) -tak samo jak na cppreference,
 max_size O(1) -tak samo jak na cppreference,
 clear O(N) -tak samo jak na cppreference,
 insert O(1) -tak samo jak na cppreference /normalny przypadek, bo w pesymistycznym moze byc O(N)/,
 erase O(1) -tak samo jak na cppreference /normalny przypadek, bo w pesymistycznym moze byc O(N)/,
 swap O(1) -tak samo jak na cppreference,
 at O(1) -tak samo jak na cppreference /normalny przypadek, bo w pesymistycznym moze byc O(N)/,
 operator[] O(1) -tak samo jak na cppreference /normalny przypadek, bo w pesymistycznym moze byc O(N)/,
 count O(1) -tak samo jak na cppreference /normalny przypadek, bo w pesymistycznym moze byc O(N), co widac po bledzie 37%/,
 find O(1) -tak samo jak na cppreference /normalny przypadek, bo w pesymistycznym moze byc O(N)/,
 equal_range O(1) -tak samo jak na cppreference /normalny przypadek, bo w pesymistycznym moze byc O(N)/,
 rehash O(N) -tak samo jak na cppreference /normalny przypadek, bo w pesymistycznym moze byc O(N^2), co widac po bledzie/,
 reserve O(N) -tak samo jak na cppreference /normalny przypadek, bo w pesymistycznym moze byc O(N^2), co widac po bledzie/,
 
 
Benchmarki Small w trybie Release (te same zlozonosci co w trybie debug oprocz wymienionych):

 List: Insert O(logN) - tutaj otrzymalismy dopuszczalna zlozonosc (pesymistyczna dla insert)
 
 
Benchmarki dla Medium Debug - te same zlozonoscico Small Debug oprocz:

 List: Reverse O(N^2)
 
 Multiset: erase O(1)-mozliwy blad pomiarowy benchmarkow
            count O(1)
 
Benchmarki dla Large Debug - te same zlozonosci co Small Debug oprocz:

 Multiset: erase O(1)-mozliwy blad pomiarowy benchmarkow
           count O(1)-
 
 Unordered_map: Reserve O(1)
 

Dla Large Release/Medium Release te same zlozonosci co dla trybu Debug dla tego samego rozmiaru.


D. 
W przypadku funkcji add nalezy po dodaniu elementu go usunac, o ile nie wplywa to na zlozonosc (zlozonosc bedzie ta sama tylko ewentualnie 2xwieksza stala)
Tak samo sytuacja wyglada w przypadku remove. Inna sytuacja jest w przypadku czyszczenia calej struktury, gdyz po kazdym przejsciu trzeba przywrocic ta sama
ilosc elementow. I tu najlatwiej zobaczyc na przykladzie list.clear(). Po wyczyszczeniu n elementow zlozonosc wynosi O(n), jednak trzeba ponownie wstawic 
elementy i tam juz otrzymujemy zlozonosc O(nlogn) (n*insert, ktory moze miec zlonosc O(logn)). Abysmy otrzymali zlozonosc jakiej oczekujemy to korzystamy
z Pause.state() i Resume.state(), ktore spowoduja ze czas pomiedzy tymi funkcjami nie bedzie liczony do ogolnej zlozonosci.


ZAD 5.

A. Tryb Release ma zapewnic szybsze dzialanie przez pominiecie linii z niuzywanymi wartosciami (wiekszosc kompilatarow inicjalizuje wart 0x00 przez co w trybie release sa one pomijane)

B. DoNotOptimaze(...) zapewni dam ze w trybie Release jakis fragment kodu sie wykona i nie zostanie pominiety nawet jak jego wartosc nie jest potem wykorzystywana
Dosyc podobnie dziala ClobberMemory(), jednak w tym przypadku nie podajemy parametru ktory ma byc zapisany i odczytany bezposrednio z pamieci.

C. Napisane odpowiedz o zlozonosciach w 4C

ZAD 6. 

A. Napisana odpowiedz o zlozonosciach w 4C

B. Czas wykonania benchmarkow jest wiekszy. 
Przy wiekszych typach mozemy sie spodziewac bledow pamieciowych, przez co proces wykonujacy benchmark sie zawiesi lub zostanie zabity.
Nie moze byc wytlumaczone implementacja operatorow, gdyz na czas dzialania wplywa tez implementacja samych struktur. 

ZAD 7. 

Hardware:
Run on (2 X 2712 MHz CPU s)
CPU Caches:
  L1 Data 32 KiB (x2)
  L1 Instruction 32 KiB (x2)
  L2 Unified 256 KiB (x2)
  L3 Unified 3072 KiB (x2)
Load Average: 0.32, 0.35, 0.48

Na podstawie powyzszych obserwacji mozna stwierdzic ze w normalnym uzyciu tryb Release jest o wiele lepszy niz tryb Debug. Nie dosc ze jest szybszy 
przez optymalizacje kodu to rowniez przy uzyciu funkcji DoNotOptimize i ClobberMemory zwraca zlozonosci blizej oczekiwanych niz najbardziej optymalnych.
