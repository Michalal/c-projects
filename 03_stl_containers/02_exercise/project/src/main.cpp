#include <iostream>
#include "Calculator.h"

int main()
{
    Calculator calculator{1, 2};

    std::cout << "Sum: " << calculator.add() << std::endl;

    return 0;
}