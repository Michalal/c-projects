#include <gtest/gtest.h>

#include <set>
#include <map>
#include <unordered_set>
using namespace std;
TEST(SetString, RemoveElements) {

    // TODO: ...
    set <string> set({"A","B","C"});
    ASSERT_EQ(3u, set.size());
    EXPECT_EQ(1u, set.count("A"));
    EXPECT_EQ(1u, set.count("B"));
    EXPECT_EQ(1u, set.count("C"));

    // TODO: ...
    set.erase("B");
    ASSERT_EQ(2u, set.size());
    EXPECT_EQ(1u, set.count("A"));
    EXPECT_EQ(0u, set.count("B"));
    EXPECT_EQ(1u, set.count("C"));
}

TEST(SetFloat, ElementsSortedWhenIterating) {

    // TODO: ...
    set <double> set;
    ASSERT_TRUE(set.empty());

    // TODO: ...
    set.insert(3.2f);
    ASSERT_EQ(1u, set.size());
    EXPECT_EQ(1u, set.count(3.2f));

    // TODO: ...
    set.insert(1.1f);
    set.insert(4.1f);
    ASSERT_EQ(3u, set.size());
    EXPECT_EQ(1u, set.count(3.2f));
    EXPECT_EQ(1u, set.count(4.1f));
    EXPECT_EQ(1u, set.count(1.1f));

    // TODO: ...
    set.insert(0.3f);
    auto iter = set.begin();
    EXPECT_FLOAT_EQ(0.3f, *iter++);
    EXPECT_FLOAT_EQ(1.1f, *iter++);
    EXPECT_FLOAT_EQ(3.2f, *iter++);
    EXPECT_FLOAT_EQ(4.1f, *iter++);
    EXPECT_EQ(set.end(), iter);
}

TEST(SetInt, ElementsSortedInDecreasingOrderWhenIterating) {

    struct Comparator {
        // TODO: ...
        bool operator()(int a, int b)
        {
            return a>b;
        }
    };

    // TODO: ...
    set <int, Comparator> set;
    //bez struct set <int, ::greater <int> > set;
    set.insert(3);
    set.insert(2);
    set.insert(1);
    auto iter = set.begin();
    EXPECT_EQ(3, *iter++);
    EXPECT_EQ(2, *iter++);
    EXPECT_EQ(1, *iter++);
    EXPECT_EQ(set.end(), iter);

    // TODO: ...
    set.insert(4);
    iter = set.begin();
    EXPECT_EQ(4, *iter++);
    EXPECT_EQ(3, *iter++);
    EXPECT_EQ(2, *iter++);
    EXPECT_EQ(1, *iter++);
    EXPECT_EQ(set.end(), iter);
}

TEST(SetChar, InvalidComparatorThatCausesOnlyOneElementToBeInserted) {

    struct Comparator {
        // TODO: ...
        bool operator()(char a, char b)
        {
            return 0;
        }
    };

    // TODO: ...
    set <char,Comparator> set;
    ASSERT_TRUE(set.empty());

    // TODO: ...
    set.insert('u');
    ASSERT_EQ(1u, set.size());

    // TODO: ...
    set.insert('d');
    ASSERT_EQ(1u, set.size());
}

TEST(SetChar, InvalidComparatorThatCausesMultipleCopiesOfTheSameElementToBeInserted) {

    struct Comparator {
        // TODO: ...
        bool operator()(char a, char b)
        {
            if(a!=b)
                return a>b;
            return a==b;
        }
    };

    // TODO: ...
    set <char, Comparator> set;
    ASSERT_TRUE(set.empty());

    // TODO: ...
    set.insert('u');
    ASSERT_EQ(1u, set.size());

    // TODO: ...
    set.insert('u');
    ASSERT_EQ(2u, set.size());
}

TEST(SetValue, CustomTypeAndComparator) {

    struct Value {
        // TODO: ...
        int p;
        int q;
    };

    struct Comparator {
        // TODO: ...
        bool operator()(const Value& a, const Value& b) const {
            return (a.p<b.p || a.q<b.q);
        }
    };

    // TODO: ...
    set <Value, Comparator> set;
    ASSERT_TRUE(set.empty());

    // TODO: ...
    set.insert(Value{1,0});
    ASSERT_EQ(1u, set.size());
    EXPECT_EQ(1u, set.count(Value{1, 0}));

    // TODO: ...
    set.insert(Value{2,0});
    ASSERT_EQ(2u, set.size());
    EXPECT_EQ(1u, set.count(Value{1, 0}));
    EXPECT_EQ(1u, set.count(Value{2, 0}));

    // TODO: ...
    set.insert(Value{1,1});
    ASSERT_EQ(3u, set.size());
    EXPECT_EQ(1u, set.count(Value{1, 0}));
    EXPECT_EQ(1u, set.count(Value{2, 0}));
    EXPECT_EQ(1u, set.count(Value{1, 1}));

    // TODO: ...
    set.insert(Value{2,2});
    ASSERT_EQ(4u, set.size());
    EXPECT_EQ(1u, set.count(Value{1, 0}));
    EXPECT_EQ(1u, set.count(Value{2, 0}));
    EXPECT_EQ(1u, set.count(Value{1, 1}));
    EXPECT_EQ(1u, set.count(Value{2, 2}));
}

TEST(SetInt, ElementsNotLessThanGivenValue) {

    // TODO: ...
    set <int> set ({1,2,3,4,5});
    ASSERT_EQ(5u, set.size());
    EXPECT_EQ(1u, set.count(1));
    EXPECT_EQ(1u, set.count(2));
    EXPECT_EQ(1u, set.count(3));
    EXPECT_EQ(1u, set.count(4));
    EXPECT_EQ(1u, set.count(5));

    // TODO: ...
    auto iter = set.begin();
    ::advance(iter,2);
    EXPECT_EQ(3, *iter++);
    EXPECT_EQ(4, *iter++);
    EXPECT_EQ(5, *iter++);
    EXPECT_EQ(set.end(), iter);
}

TEST(MapStringString, CreateUsingInitializerList) {

    // TODO: ...
    map <string, string> map {{"PL","Poland"},{"JP","Japan"},{"DE","Germany"}};
    ASSERT_EQ(3u, map.size());
    EXPECT_EQ("Poland", map["PL"]);
    EXPECT_EQ("Japan", map["JP"]);
    EXPECT_EQ("Germany", map["DE"]);
}

TEST(MapSetString, NestedCollections) {

    // TODO: ...
    map <  string, set <string> > map;
    ASSERT_TRUE(map.empty());

    // TODO: ...
    map["digits"].insert("1");
    ASSERT_EQ(1u, map.size());
    ASSERT_EQ(1u, map.count("digits"));
    map["digits"].insert("2");
    map["digits"].insert("3");
    map["digits"].insert("4");
    ASSERT_EQ(4u, map["digits"].size());
    EXPECT_EQ(1u, map["digits"].count("1"));
    EXPECT_EQ(1u, map["digits"].count("2"));
    EXPECT_EQ(1u, map["digits"].count("3"));
    EXPECT_EQ(1u, map["digits"].count("4"));
}

TEST(MultimapIntInt, RemoveRangeOfElements) {

    // TODO: ...
    multimap <int,int> multimap;
    multimap.insert({1,1});
    multimap.insert({2,1});
    multimap.insert({2,2});
    multimap.insert({2,3});
    multimap.insert({2,4});
    multimap.insert({3,1});
    multimap.insert({3,2});
    multimap.insert({3,3});

    ASSERT_EQ(8, multimap.size());
    EXPECT_EQ(1, multimap.count(1));
    EXPECT_EQ(4, multimap.count(2));
    EXPECT_EQ(3, multimap.count(3));

    // TODO: ...
    multimap.erase(2);
    ASSERT_EQ(4, multimap.size());
    EXPECT_EQ(1, multimap.count(1));
    EXPECT_EQ(3, multimap.count(3));
}

TEST(UnorderedSetValue, CustomTypeHashAndComparator) {

    struct Value {
        // TODO: ...
        int a;
        int b;
    };

    struct Hash {
        // TODO: ...
        size_t operator()(const Value& value) const {
            hash<int> hash;
            return hash(value.a);
        }
    };

    struct Equal {
        // TODO: ...
        bool operator()(const Value& lhs, const Value& rhs) const {
            return (lhs.a==rhs.a && lhs.b==rhs.b);
        }
    };

    // TODO: ...
    unordered_set <Value, Hash, Equal> unordered_set;
    ASSERT_TRUE(unordered_set.empty());

    // TODO: ...
    unordered_set.insert(Value{1, 1});
    ASSERT_EQ(1u, unordered_set.size());
    EXPECT_EQ(1u, unordered_set.count(Value{1, 1}));

    // TODO: ...
    unordered_set.insert(Value{1, 1});
    ASSERT_EQ(1u, unordered_set.size());
    EXPECT_EQ(1u, unordered_set.count(Value{1, 1}));

    // TODO: ...
    unordered_set.insert(Value{1, 2});
    unordered_set.insert(Value{2, 1});
    unordered_set.insert(Value{2, 2});
    ASSERT_EQ(4u, unordered_set.size());
    EXPECT_EQ(1u, unordered_set.count(Value{1, 1}));
    EXPECT_EQ(1u, unordered_set.count(Value{1, 2}));
    EXPECT_EQ(1u, unordered_set.count(Value{2, 1}));
    EXPECT_EQ(1u, unordered_set.count(Value{2, 2}));
}

TEST(UnorderedSetInt, BucketsAndLoadFactor) {

    // TODO: ...
    unordered_set <int> unordered_set({1});
    ASSERT_EQ(1u, unordered_set.size());
    EXPECT_EQ(2u, unordered_set.bucket_count());
    EXPECT_FLOAT_EQ(1.0 / 2.0, unordered_set.load_factor());
    EXPECT_FLOAT_EQ(1.0f, unordered_set.max_load_factor());

    // TODO: ...
    unordered_set.insert({2});
    ASSERT_EQ(2u, unordered_set.size());
    EXPECT_EQ(2u, unordered_set.bucket_count());
    EXPECT_FLOAT_EQ(1.0f, unordered_set.load_factor());
    EXPECT_FLOAT_EQ(1.0f, unordered_set.max_load_factor());

    // TODO: ...
    unordered_set.insert({3});
    ASSERT_EQ(3u, unordered_set.size());
    EXPECT_EQ(5u, unordered_set.bucket_count());
    EXPECT_FLOAT_EQ(3.0 / 5.0, unordered_set.load_factor());
    EXPECT_FLOAT_EQ(1.0f, unordered_set.max_load_factor());

    // TODO: ...
    unordered_set.insert({7});
    ASSERT_EQ(4u, unordered_set.size());
    EXPECT_EQ(5u, unordered_set.bucket_count());
    EXPECT_FLOAT_EQ(4.0 / 5.0, unordered_set.load_factor());
    EXPECT_FLOAT_EQ(1.0f, unordered_set.max_load_factor());

    // TODO: ...
    unordered_set.insert({11});
    ASSERT_EQ(5u, unordered_set.size());
    EXPECT_EQ(5u, unordered_set.bucket_count());
    EXPECT_FLOAT_EQ(1.0f, unordered_set.load_factor());
    EXPECT_FLOAT_EQ(1.0f, unordered_set.max_load_factor());

    // TODO: ...
    unordered_set.insert({21});
    unordered_set.insert({37});
    unordered_set.insert({14});
    unordered_set.insert({88});
    unordered_set.insert({33});
    ASSERT_EQ(10u, unordered_set.size());
    EXPECT_EQ(11u, unordered_set.bucket_count());
    EXPECT_FLOAT_EQ(10.0 / 11.0, unordered_set.load_factor());
    EXPECT_FLOAT_EQ(1.0f, unordered_set.max_load_factor());

    // TODO: ...
    unordered_set.insert({10});
    ASSERT_EQ(11u, unordered_set.size());
    EXPECT_EQ(11u, unordered_set.bucket_count());
    EXPECT_FLOAT_EQ(11.0 / 11.0, unordered_set.load_factor());
}
