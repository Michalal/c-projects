//
// Created by student on 12/11/20.
//

#ifndef PROJECT_OBSERVERIMPL_H
#define PROJECT_OBSERVERIMPL_H
#include "Observer.h"
#include <map>

class ObserverImpl : public Observer {
public:

    void remember(std::string question, std::string answer);
    std::string answer(std::string question) const;

private:
    std::map<std::string, std::string> answers;
};
#endif //PROJECT_OBSERVERIMPL_H
