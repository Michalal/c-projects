//
// Created by student on 12/11/20.
//

#ifndef PROJECT_TIMEIMPL_H
#define PROJECT_TIMEIMPL_H
#include "Time.h"
class TimeImpl : public Time {
public:

    void create(bool endless) override;
    long long now() const override;

    void flow() override;

private:
    long long year;
    bool endless;
};
#endif //PROJECT_TIMEIMPL_H
