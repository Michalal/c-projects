//
// Created by student on 12/11/20.
//

#ifndef PROJECT_SPACEIMPL_H
#define PROJECT_SPACEIMPL_H
#include "Space.h"
class SpaceImpl : public Space{
public:

    void create(unsigned int dimensions);
    unsigned int dimensions() const;

private:
    unsigned int dimensionsCount;
    std::vector<Point> points;
};
#endif //PROJECT_SPACEIMPL_H
