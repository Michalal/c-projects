#include "Universe.h"
#include "TestIncludes.h"

class Socket {
public:

    virtual ~Socket() = default;

    virtual std::string receive() const = 0;
    virtual void send(const std::string& message) = 0;
};

class Server {
public:

    explicit Server(Socket& socket) : socket(socket) {}

    void run(int count) const {

        for (auto i=0u; i<count; i++) {

            auto request = socket.receive();

            auto response = "Hello! The request was: " + request;

            socket.send(response);
        }
    }

private:
    Socket& socket;
};

class SocketMock : public Socket {
public:

    MOCK_CONST_METHOD0(receive, std::string());
    MOCK_METHOD1(send, void(const std::string& message));
};

class TimeMock : public Time {
public:
    MOCK_METHOD(long long, now,(), (const,override));
    MOCK_METHOD(void, create, (bool), (override));
    MOCK_METHOD(void,flow,(),(override));
};

class SpaceMock : public Space {
public:
    MOCK_METHOD(void,create,(unsigned int),(override));
    MOCK_METHOD(unsigned int,dimensions,(),(const,override));
};

class ObserverMock : public Observer {
public:
    MOCK_METHOD(void,remember,(std::string question, std::string answer),(override));
    MOCK_METHOD(std::string,answer,(std::string question),(const,override));
};
TEST(Server, ShouldAddHelloToTheRequestAndSendResponse) {

    StrictMock<SocketMock> socket{};
    Server server{socket};

    EXPECT_CALL(socket, receive()).WillOnce(Return("TEST"));
    EXPECT_CALL(socket, send("Hello! The request was: TEST"));

    server.run(1);

}

TEST(Universe, Create)
{
    StrictMock<TimeMock> time{};
    StrictMock<SpaceMock> space{};
    StrictMock<ObserverMock> observer{};
    Universe universe{time,space,observer};
    EXPECT_CALL(time,create(true));
    EXPECT_CALL(space, create(11));
    EXPECT_CALL(space, dimensions()).Times(2).WillRepeatedly(Return(11));
    EXPECT_CALL(observer, remember("How many dimensions there are?", std::to_string(space.dimensions())));
    universe.create();

    // TODO: Test Universe class...
}

TEST(Universe, Earth)
{
    StrictMock<TimeMock> time{};
    StrictMock<SpaceMock> space{};
    StrictMock<ObserverMock> observer{};
    Universe universe(time, space, observer);

    EXPECT_CALL(time, now()).WillOnce(Return(9300000000)).WillOnce(Return(9300000001));
    EXPECT_CALL(observer,remember("Is there planet Earth?", "Yes!"));
    EXPECT_CALL(time,flow());
    universe.simulate(9300000001);
}

TEST(Universe, Life)
{
    StrictMock<TimeMock> time{};
    StrictMock<SpaceMock> space{};
    StrictMock<ObserverMock> observer{};
    Universe universe(time, space, observer);

    EXPECT_CALL(time, now()).WillOnce(Return(9900000000)).WillOnce(Return(9900000001));
    EXPECT_CALL(observer,remember("Does life exist?", "Yes!"));
    EXPECT_CALL(time, flow());

    universe.simulate(9900000001);
}

TEST(Universe, People)
{
    StrictMock<TimeMock> time{};
    StrictMock<SpaceMock> space{};
    StrictMock<ObserverMock> observer{};
    Universe universe(time, space, observer);

    EXPECT_CALL(time, now()).WillOnce(Return(13800000000)).WillOnce(Return(13800000001));
    EXPECT_CALL(observer,remember("Have People evolved?", "Yes!"));
    EXPECT_CALL(time, flow());

    universe.simulate(13800000001);
}
